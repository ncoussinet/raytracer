//
// Keyboardsymbdef.hh for generic in /home/coussi_n/backup/coussi_n/mlx_cpp
//
// Made by Nelson Coussinet
// Login   <coussi_n@coussi-n-pc>
//
// Started on Thu Mar  6 12:47:24 2014 Nelson Coussinet
// Last update Tue Mar 25 15:53:15 2014 Nelson Coussinet
//

#ifndef		KEYBOARDSYMBDEF_HH_
# define	KEYBOARDSYMBDEF_HH_

# include	<vector>
# include	"IEvent.hh"
# include	"Key.hh"

namespace	Xt
{
  class		Keyboard : public IEvent
  {
    public:

      Keyboard(void);
      ~Keyboard(void) throw();

      void	setCode(XEvent *);

      Key			code;

    private:
      XEvent			*ev;
      std::vector<int>		event;
      unsigned int		evSym;
  };
}

#endif				/* !KEYBOARDSYMBDEF_HH_ */
