//
// Key.hh for generic in /home/coussi_n/backup/coussi_n/mlx_cpp
//
// Made by Nelson Coussinet
// Login   <coussi_n@coussi-n-pc>
//
// Started on Tue Mar 25 13:31:38 2014 Nelson Coussinet
// Last update Tue Apr  1 09:59:48 2014 Nelson Coussinet
//

#ifndef		KEY_HH_
# define	KEY_HH_

# define	KEY_MAX 65536

namespace	Xt
{
  enum		Key
  {
    Unknown = 0,
    A = 1,
    B,
    C,
    D,
    E,
    F,
    G,
    H,
    I,
    J,
    K,
    L,
    M,
    N,
    O,
    P,
    Q,
    R,
    S,
    T,
    U,
    V,
    W,
    X,
    Y,
    Z,
    Esc
  };
}

#endif		/* !KEY_HH_ */
