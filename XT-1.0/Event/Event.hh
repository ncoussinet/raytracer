//
// Event.hh for generic in /home/coussi_n/backup/coussi_n/mlx_cpp
//
// Made by Nelson Coussinet
// Login   <coussi_n@coussi-n-pc>
//
// Started on Thu Mar 13 16:45:53 2014 Nelson Coussinet
// Last update Tue Apr  1 09:59:12 2014 Nelson Coussinet
//

#ifndef				EVENT_HH_
# define			EVENT_HH_

# include			"Keyboardsymbdef.hh"

namespace			Xt
{
  class				Win;
  class				Keyboard;

  class				Event
  {
    public:
      Event(void);
      Event(Win *);
      ~Event(void) throw();

      void			GetEvent(void);
      void			setEvent(Win *);

      void			putEvent(void);

      int			type;
      Keyboard			*key;

    private:
      XEvent			*ev;
      Win			*_win;
  };
}

#endif				/* !EVENT_HH_ */
