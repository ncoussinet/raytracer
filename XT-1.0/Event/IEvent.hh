//
// IEvent.hh for generic in /home/coussi_n/backup/coussi_n/mlx_cpp/Event
//
// Made by Nelson Coussinet
// Login   <coussi_n@coussi-n-pc>
//
// Started on Tue Mar 25 12:25:37 2014 Nelson Coussinet
// Last update Tue Apr  1 10:16:00 2014 Nelson Coussinet
//

#ifndef		IEVENT_HH_
# define	IEVENT_HH_

# include	"../Window.hh"

namespace	Xt
{
  class		IEvent
  {
    public:
      virtual ~IEvent(void) {};

      virtual void setCode(XEvent *) = 0;
  };
}

#endif		/* IEVENT_HH_ */
