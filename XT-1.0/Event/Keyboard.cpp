//
// Keyboard.cpp for  in /home/coussi_n/backup/coussi_n/mlx_cpp/Event
//
// Made by Nelson Coussinet
// Login   <coussi_n@epitech.net>
//
// Started on Thu Mar 13 13:00:04 2014 Nelson Coussinet
// Last update Tue Mar 25 16:02:54 2014 Nelson Coussinet
//

#include	<X11/keysymdef.h>
#include	<X11/XKBlib.h>
#include	"Keyboardsymbdef.hh"

#include	<iostream>

namespace	Xt
{
  Keyboard::Keyboard(void)
  {
    event.resize(KEY_MAX, Unknown);
    event[XK_Escape] = Esc;
    event[A] = XK_A;
    event[B] = XK_B;
    event[C] = XK_C;
    event[D] = XK_D;
    event[E] = XK_E;
    event[F] = XK_F;
    event[G] = XK_G;
    event[H] = XK_H;
    event[I] = XK_I;
    event[J] = XK_J;
    event[K] = XK_K;
    event[L] = XK_L;
    event[M] = XK_M;
    event[N] = XK_N;
    event[O] = XK_O;
    event[P] = XK_P;
    event[Q] = XK_Q;
    event[R] = XK_R;
    event[S] = XK_S;
    event[T] = XK_T;
    event[U] = XK_U;
    event[V] = XK_V;
    event[W] = XK_W;
    event[X] = XK_X;
    event[Y] = XK_Y;
    event[Z] = XK_Z;
    code = Unknown;
  }

  Keyboard::~Keyboard(void) throw()
  {

  }

  void		Keyboard::setCode(XEvent *tmp)
  {
    char	buffer[32];
    KeySym	symbol;

    XLookupString(&tmp->xkey, buffer, 32, &symbol, NULL);
    if (KEY_MAX <= symbol)
      this->code = Unknown;
    else
      this->code = (Key)event[symbol];
  }
}
