//
// Event.cpp for  in /home/coussi_n/backup/coussi_n/mlx_cpp
//
// Made by Nelson Coussinet
// Login   <coussi_n@epitech.net>
//
// Started on Thu Mar 13 16:41:39 2014 Nelson Coussinet
// Last update Tue Apr  1 09:59:32 2014 Nelson Coussinet
//

#include	"Event.hh"
#include	<iostream>

namespace	Xt
{
  Event::Event(void)
  {
    ev = new XEvent;
    key = new Keyboard;
  }

  Event::Event(Win *win)
  {
    this->_win = win;
    ev = new XEvent;
    key = new Keyboard;
    XNextEvent(win->dpy, ev);
  }


  Event::~Event(void) throw()
  {
    delete ev;
  }

  void		Event::setEvent(Win *win)
  {
    this->_win = win;
    XNextEvent(win->dpy, ev);
  }

  void		Event::putEvent(void)
  {
    XNextEvent(_win->dpy, ev);
  }

  void		Event::GetEvent(void)
  {
    int		n;

    n = XEventsQueued(_win->dpy, QueuedAfterReading);
    if (n > 0)
      {
	XNextEvent(_win->dpy, ev);
	this->type = ev->type;
	if (ev->type == ClientMessage)
	  this->_win->close = false;
	else if (ev->type == ConfigureNotify)
	  {
	    XConfigureEvent XWin = ev->xconfigure;
	    this->_win->width = XWin.width;
	    this->_win->height = XWin.height;
	  }
	else if (ev->type == KeyPress)
	  {
	    key->setCode(ev);
	  }
      }
  }
}
