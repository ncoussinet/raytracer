//
// test.hh for generic in /home/coussi_n//mlx_cpp
//
// Made by nelson coussinet
// Login   <coussi_n@pc-coussi_n>
//
// Started on Thu Nov 21 14:52:22 2013 nelson coussinet
// Last update Tue Apr  1 10:00:37 2014 Nelson Coussinet
//

#ifndef			WINDOW_HH
# define		WINDOW_HH

# include		<X11/Xutil.h>
# include		<stdexcept>
# include		"Event/Event.hh"
# include		"Images/layer.hh"

namespace		Xt
{
  class			Event;
  class			Layer;

  class			Win
  {
    public:

      Win(void);
      Win(int, int, std::string);
      ~Win(void) throw();

      bool		isOpen(void);
      bool		setFramesLimit(int);

      void		setEvent(Event *);
      Event		*setEvent(void);

      void		draw(Layer &);
    // private:


      int		width;
      int		height;
      std::string	name;
      Window		win;
      Display		*dpy;
      Atom		delWindow;
      Event		*_ev;
      bool		close;
      int		frames;
      time_t		time_new;
      time_t		time_prev;
      double		timeF;
      Visual		*visual;
      int		screen;
      int		depth;
      Window		root;
      Pixmap		pix;
      GC		gc;
  };
}

#endif			/* !WINDOW_HH_ */
