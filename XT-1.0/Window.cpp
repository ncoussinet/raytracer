//
// Mlx.cpp for  in /home/coussi_n//mlx_cpp
//
// Made by nelson coussinet
// Login   <coussi_n@epitech.net>
//
// Started on Thu Nov 21 14:54:53 2013 nelson coussinet
// Last update Wed May  7 17:20:46 2014 Nelson Coussinet
//

#include	<iostream>
#include	<unistd.h>
#include	"Window.hh"

namespace	Xt
{
  Win::Win(void)
  {
    this->close = true;
    if ((this->dpy = XOpenDisplay("")))
      {
	XGCValues	xgcv;

	this->screen = DefaultScreen(this->dpy);
	this->root = DefaultRootWindow(this->dpy);
	this->win = XCreateSimpleWindow(dpy, this->root,
					0, 0, 800, 600, 6,
					BlackPixel(dpy, this->screen),
					WhitePixel(dpy, this->screen));
	XStoreName(dpy, win, "Proj");
	XSetIconName(dpy, win, "Proj");
	this->delWindow = XInternAtom(dpy, "WM_DELETE_WINDOW", 0);
	XSetWMProtocols(dpy, win, &delWindow, 1);
	XSelectInput(dpy, win, ExposureMask | KeyPressMask | ButtonPressMask | StructureNotifyMask);
	XMapWindow(dpy, win);
	this->name = "Proj";
	this->width = 800;
	this->height = 600;
	this->frames = 0;
	this->visual = DefaultVisual(this->dpy, this->screen);
	this->depth = DefaultDepth(this->dpy, this->screen);
	this->pix = XCreatePixmap(this->dpy, this->root, this->width, this->height, this->depth);
	xgcv.foreground = -1;
	xgcv.function = GXcopy;
	xgcv.plane_mask = AllPlanes;
	this->gc = XCreateGC(this->dpy, this->win, GCFunction | GCPlaneMask | GCForeground, &xgcv);
      }
    else
      throw std::runtime_error("ERROR TOTO");
  }

  Win::Win(int my_width, int my_height, std::string my_name)
  {
    this->close = true;
    if ((dpy = XOpenDisplay("")))
      {
	XGCValues	xgcv;

	this->screen = DefaultScreen(this->dpy);
	this->root = DefaultRootWindow(this->dpy);
	this->win = XCreateSimpleWindow(dpy, DefaultRootWindow(dpy),
				  0, 0, my_width, my_height, 6,
				  BlackPixel(dpy, DefaultScreen(dpy)),
				  WhitePixel(dpy, DefaultScreen(dpy)));
	XStoreName(dpy, win, my_name.c_str());
	XSetIconName(dpy, win, my_name.c_str());
	delWindow = XInternAtom(dpy, "WM_DELETE_WINDOW", 0);
	XSetWMProtocols(dpy, win, &delWindow, 1);
	XSelectInput(dpy, win, ExposureMask | KeyPressMask | ButtonPressMask | StructureNotifyMask);
	XMapWindow(dpy, win);
	this->width = my_width;
	this->height = my_height;
	this->name = my_name;
	this->frames = 0;
	this->visual = DefaultVisual(this->dpy, this->screen);
	this->depth = DefaultDepth(this->dpy, this->screen);
	this->pix = XCreatePixmap(this->dpy, this->root, this->width, this->height, this->depth);
	xgcv.foreground = -1;
	xgcv.function = GXcopy;
	xgcv.plane_mask = AllPlanes;
	this->gc = XCreateGC(this->dpy, this->win, GCFunction | GCPlaneMask | GCForeground, &xgcv);
	time(&this->time_prev);
      }
    else
      throw std::runtime_error("ERROR INIT");
  }

  Win::~Win(void) throw()
  {
  }

  void		Win::setEvent(Event *ev)
  {
    ev->setEvent(this);
    this->_ev = ev;
  }

  Event		*Win::setEvent(void)
  {
    this->_ev = new Event(this);
    return (_ev);
  }

////////////////////////////////////////////////////////////////////////////////
// to implement if necesary
// Win::Win(int my_width, int my_height, std::string my_name)
// {
//   if ((dpy = XOpenDisplay("")))
//     {
//       win = XCreateSimpleWindow(dpy, DefaultRootWindow(dpy),
// 				0, 0, my_width, my_height, 6,
// 				BlackPixel(dpy, DefaultScreen(dpy)),
// 				WhitePixel(dpy, DefaultScreen(dpy)));
//       XStoreName(dpy, win, my_name);
//       XSetIconName(dpy, win, my_name);
//       delWindow = XInternAtom(dpy, "WM_DELETE_WINDOW", 0);
//       XSetWMProtocols(dpy, win, &delWindow, 1);
//       XSelectInput(dpy, win, ExposureMask | KeyPressMask | ButtonPressMask | StructureNotifyMask);
//       XMapWindow(dpy, win);
//     }
//   else
//     throw std::runtime_error("ERROR INIT");
// }
////////////////////////////////////////////////////////////////////////////////

  bool		Win::setFramesLimit(int size)
  {
    if (size < 0)
      return (false);
    this->frames = size;
    this->timeF = 1.0 / (double)size;
    return (true);
  }

  bool		Win::isOpen(void)
  {
    int		timediff;

    time(&this->time_new);
    if (this->frames > 0)
      {
	timediff = difftime(this->time_new, this->time_prev);
	if (timediff < this->timeF)
	  usleep((this->timeF - timediff) * 1000000);
      }
    time(&this->time_prev);
    time(&this->time_new);
    return (this->close);
  }

  void		Win::draw(Layer &layer)
  {
    if (layer.first == true || _ev->type == ConfigureNotify)
      {
	XPutImage(this->dpy, this->pix, this->gc, layer.getImg(), 0, 0, 0, 0,
		  layer.getLenght(), layer.getHeight());
	XCopyArea(this->dpy, this->pix, this->win, this->gc, 0, 0,
		  layer.getLenght(), layer.getHeight(), 0, 0);
	this->_ev->putEvent();
	layer.first = false;
      }
  }
}
