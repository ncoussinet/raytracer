//
// layer.cpp for  in /home/coussi_n/backup/coussi_n/mlx_cpp/Images
//
// Made by Nelson Coussinet
// Login   <coussi_n@epitech.net>
//
// Started on Tue Mar 25 17:29:18 2014 Nelson Coussinet
// Last update Sun Apr  6 13:38:03 2014 Nelson Coussinet
//

#include	"layer.hh"

namespace	Xt
{
  Layer::Layer(void)
  {
    this->_lenght = 0;
    this->_height = 0;
    this->first = true;
  }

  Layer::Layer(Win *win, int lenght, int height)
  {
    this->first = true;
    this->_win = win;
    this->_lenght = lenght;
    this->_height = height;
    this->data = new char[lenght * height * 4];
    if ((this->img = XCreateImage(this->_win->dpy, this->_win->visual, this->_win->depth,
				  ZPixmap, 0, this->data, lenght, height, 32, 0)) == NULL)
      {
	delete data;
	throw std::runtime_error("error XCreate Image failed");
      }
  }

  Layer::~Layer(void) throw()
  {
  }

  void		Layer::setLayer(Win *win, int lenght, int height)
  {
    this->_win = win;
    this->_lenght = lenght;
    this->_height = height;
    if (!data)
      delete data;
    this->data = new char[lenght * height * 4];
    if ((this->img = XCreateImage(this->_win->dpy, this->_win->visual, this->_win->depth,
				  ZPixmap, 0, this->data, lenght, height, 32, 0)) == NULL)
      {
	delete this->data;
	throw std::runtime_error("error XCreate Image failed");
      }
    this->val_bpp = img->bits_per_pixel / 8;
    this->sizeline = img->bytes_per_line;
  }

  XImage	*Layer::getImg(void)
  {
    return (this->img);
  }

  int		Layer::getLenght(void)
  {
    return (this->_lenght);
  }

  int		Layer::getHeight(void)
  {
    return (this->_height);
  }

  void		Layer::pixelPut(int x, int y, unsigned int color)
  {
    int		i;
    int		pos_init;

    i = -1;
    if (x < 0 || y < 0 ||
	x >= this->_lenght || y >= this->_height)
      return ;
    pos_init = y * sizeline + x * this->val_bpp;
    // data[pos_init] = color & 0xFF;
    // data[pos_init + 1] =  (color & 0xFF00) >> 8;
    // data[pos_init + 2] = (color & 0xFF0000) >> 16;
    while (++i < this->val_bpp)
      {
    	data[pos_init + i] = ((char *)(&color))[i];
      }
    this->first = true;
  }
}
