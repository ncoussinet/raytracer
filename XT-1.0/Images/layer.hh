//
// img.hh for generic in /home/coussi_n/backup/coussi_n/mlx_cpp/Images
//
// Made by Nelson Coussinet
// Login   <coussi_n@coussi-n-pc>
//
// Started on Tue Mar 25 17:22:12 2014 Nelson Coussinet
// Last update Tue Apr  1 10:16:10 2014 Nelson Coussinet
//

#ifndef		LAYER_HH_
# define	LAYER_HH_

# include	"../Window.hh"

namespace	Xt
{
  class		Win;

  class		Layer
  {
    public:
      Layer(void);
      Layer(Win *win, int lenght, int height);
      ~Layer(void) throw();

      void	setLayer(Win *, int, int);
      void	pixelPut(int x, int y, unsigned int color);
      XImage	*getImg(void);
      int	getLenght(void);
      int	getHeight(void);
      bool	first;

    private:
      char	*data;
      Win	*_win;
      int	_lenght;
      int	_height;
      XImage	*img;
      int	val_bpp;
      int	sizeline;
  };
}

#endif		/* !LAYER_HH_ */
