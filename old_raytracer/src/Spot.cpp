//
// Spot.cpp for  in /home/coussi_n/raytracer/src/objects
//
// Made by Nelson Coussinet
// Login   <coussi_n@epitech.net>
//
// Started on Sat Apr  5 20:01:11 2014 Nelson Coussinet
// Last update Mon Apr  7 09:53:27 2014 Nelson Coussinet
//

#include	"Spot.hh"
#include	<iostream>

Spot::Spot(void)
{
  this->ParseFunc["X = "] = &Spot::SpotX;
  this->ParseFunc["Y = "] = &Spot::SpotY;
  this->ParseFunc["Z = "] = &Spot::SpotZ;
}

Spot::~Spot(void)
{
}

t_pos		&Spot::getSpotPos(void)
{
  return (this->SpotPos);
}

void		Spot::SpotX(const std::string &line)
{
  std::istringstream(line) >> this->SpotPos.x;
}

void		Spot::SpotY(const std::string &line)
{
  std::cout << line << std::endl;
}

void		Spot::SpotZ(const std::string &line)
{
  std::cout << line << std::endl;
}

bool		Spot::setSpot(std::ifstream &file, int *nb_line)
{
  int		i;
  int		size;
  std::string	line;
  std::string	call;

  i = 0;
  while (i < ELEM_MAX_SPOT && getline(file, line))
    {
      (*nb_line)++;
      if (i >= ELEM_MIN_SPOT && !line.compare("END"))
	return (true);
      if (line.compare(0, 2, "##") != 0)
	{
	  if (line.size() < 5)
	    return (false);
	  if ((size = line.find(" = ") + 3) == std::string::npos)
	    return (false);
	  call = line.substr(0, size);
	  if (!this->ParseFunc[call])
	    return (false);
	  (this->*ParseFunc[call])(line.substr(size));
	  ++i;
	}
      else
	std::cout << line.c_str() + 3 << std::endl;
    }
  return (false);
}
