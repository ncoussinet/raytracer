//
// Camera.cpp for  in /home/coussi_n/raytracer/src
//
// Made by Nelson Coussinet
// Login   <coussi_n@epitech.net>
//
// Started on Wed Apr  2 14:13:10 2014 Nelson Coussinet
// Last update Fri Apr 18 18:26:39 2014 Nelson Coussinet
//

#include	<sstream>
#include	<iostream>
#include	"Spot2.hh"

Spot2::Spot2(void)
{
  CamPos.x = 0;
  CamPos.y = 0;
  CamPos.z = 0;
  this->rgb = new t_rgb;
  this->ParseFunc["X = "] = &Spot2::CamX;
  this->ParseFunc["Y = "] = &Spot2::CamY;
  this->ParseFunc["Z = "] = &Spot2::CamZ;
  this->ParseFunc["RGB = "] = &Spot2::setRGB;
}

Spot2::~Spot2(void)
{
}

bool		Spot2::setPosition(std::ifstream &file, int *line_nb)
{
  int		i;
  int		size;
  std::string	line;
  std::string	call;

  i = 0;
  while (i < 4 && getline(file, line))
    {
      (*line_nb)++;
      if (line.compare(0, 2, "##") != 0)
	{
	  if (line.size() < 5)
	    return (false);
	  if ((size = line.find(" = ") + 3) == std::string::npos)
	    return (false);
	  call = line.substr(0, size);
	  if (!this->ParseFunc[call])
	    return (false);
	  (this->*ParseFunc[call])(line.substr(size));
	  ++i;
	}
      else
	std::cout << line.c_str() + 3 << std::endl;
    }
  if (i < 3)
    return (false);
  return (true);
}

const t_pos	&Spot2::getPosition(void) const
{
  return (this->CamPos);
}

t_rgb		*Spot2::getColor(void)
{
  return (rgb);
}

void		Spot2::CamX(const std::string &line)
{
  std::istringstream(line) >> this->CamPos.x;
}

void		Spot2::CamY(const std::string &line)
{
  std::istringstream(line) >> this->CamPos.y;
}

void		Spot2::CamZ(const std::string &line)
{
  std::istringstream(line) >> this->CamPos.z;
}

void		Spot2::setRGB(const std::string &line)
{
  std::istringstream(line) >> rgb->r >> rgb->g >> rgb->b;
}
