//
// Light.cpp for  in /home/coussi_n/raytracer/include
//
// Made by Nelson Coussinet
// Login   <coussi_n@epitech.net>
//
// Started on Thu Apr 10 10:59:44 2014 Nelson Coussinet
// Last update Mon May  5 15:05:16 2014 Nelson Coussinet
//

#include	<iostream>
#include	"Light.hh"
#include	"calc.hh"

Light::Light(void)
{
}

Light::~Light(void)
{
}

void		Light::getReflect(t_rgb *color, double reflec, t_rgb *color_obj)
{
  color->r = color_obj->r * reflec + color->r * (1.0 - reflec);
  color->g = color_obj->g * reflec + color->g * (1.0 - reflec);
  color->b = color_obj->b * reflec + color->b * (1.0 - reflec);
}

double		Light::getLightColor(t_view &vect, t_rgb *color, t_view &vect_n, t_rgb *color_tmp)
{
  double	cos_angle;

  cos_angle = mul_vect(&vect, &vect_n) / (magnitude(&vect) * magnitude(&vect_n));
  if (cos_angle < 0.0)
    cos_angle *= -1.0;
  if (cos_angle > 1.0)
    cos_angle = 1.0;
  color->r += color_tmp->r * cos_angle;
  color->g += color_tmp->g * cos_angle;
  color->b += color_tmp->b * cos_angle;
  return (cos_angle);
}

void		Light::getShine(double &shine,
				t_rgb *color,
				t_rgb *spot_color,
				double cos_angle)
{
  color->r += shine * spot_color->r * cos_angle;
  color->g += shine * spot_color->g * cos_angle;
  color->b += shine * spot_color->b * cos_angle;
}
