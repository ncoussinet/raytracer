//
// Conf.cpp for  in /home/coussi_n/raytracer/include
//
// Made by Nelson Coussinet
// Login   <coussi_n@epitech.net>
//
// Started on Mon Apr 14 10:01:47 2014 Nelson Coussinet
// Last update Mon May  5 16:58:19 2014 Nelson Coussinet
//

#include	"Conf.hh"
#include	<iostream>

Conf::Conf(void)
{
  _shadow = true;
  _light = true;
  _cartoon = true;
  this->ParseFunc["SHADOW = "] = &Conf::ConfShadow;
  this->ParseFunc["LIGHT = "] = &Conf::ConfLight;
  this->ParseFunc["CARTOON = "] = &Conf::ConfCartoon;
}

Conf::~Conf(void)
{
}

void		Conf::ConfCartoon(const std::string &line)
{
  if (!line.compare("FALSE"))
    this->_cartoon = false;
}

void		Conf::ConfShadow(const std::string &line)
{
  if (!line.compare("TRUE"))
    this->_shadow = true;
  if (!line.compare("FALSE"))
    this->_shadow = false;
}

void		Conf::ConfLight(const std::string &line)
{
  if (!line.compare("TRUE"))
    this->_light = true;
  if (!line.compare("FALSE"))
    this->_light = false;
}

bool		Conf::setConf(std::ifstream &file, int *line_nb)
{
  int		i;
  int		size;
  std::string	line;
  std::string	call;

  i = 0;
  while (i <= ELEM_MAX_CONF && getline(file, line))
    {
      (*line_nb)++;
      if (i >= ELEM_MIN_CONF && !line.compare("END"))
	return (true);
      if (line.compare(0, 2, "##") != 0)
	{
	  if (line.size() < 8)
	    return (false);
	  if ((size = line.find(" = ")) == std::string::npos)
	    return (false);
	  size += 3;
	  call = line.substr(0, size);
	  if (!this->ParseFunc[call])
	    return (false);
	  (this->*ParseFunc[call])(line.substr(size));
	  ++i;
	}
      else
	std::cout << line.c_str() + 2 << std::endl;
    }
  return (false);
}
