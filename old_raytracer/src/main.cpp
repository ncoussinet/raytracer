//
// main.cpp for  in /home/coussi_n/raytracer
//
// Made by Nelson Coussinet
// Login   <coussi_n@epitech.net>
//
// Started on Tue Apr  1 11:29:05 2014 Nelson Coussinet
// Last update Tue May  6 11:35:36 2014 Nelson Coussinet
//

#include	"raytracer.hh"
#include	<thread>
#include	<iostream>

void		ptr_func(Raytracer *ray, int nb, int nb_thread)
{
  int		xmin;
  int		xmax;

  xmin = nb * ray->getLenght() / nb_thread;
  xmax = ((nb + 1) * ray->getLenght() / nb_thread) + 1;
  if (nb == nb_thread)
    xmax--;
  ray->calc(xmin, xmax);
}

int		calc_nb_thread(void)
{
  int		nb;

  nb = 0;
  std::ifstream	file("/proc/cpuinfo", std::ios::in);

  if (file)
    {
      std::string	line;

      while (getline(file, line))
	{
	  if (line.compare(0, 9, "processor") == 0)
	    ++nb;
	}
    }
  file.close();
  return (nb);
}

int		main(int ac, char **av)
{
  Xt::Win	*win;
  Xt::Event	*ev;
  Xt::Layer	img;
  Raytracer	*ray;
  bool		exit;
  int		nb_thread;

  exit = false;
  if (ac == 1)
    ray = new Raytracer();
  else
    ray = new Raytracer(av[1]);
  if ((nb_thread = calc_nb_thread()) < 1)
    nb_thread = 1;

  std::thread	th[nb_thread];

  win = new Xt::Win(ray->getLenght(), ray->getHeight(), "toto");
  win->setFramesLimit(100);
  img.setLayer(win, ray->getLenght(), ray->getHeight());
  ray->setLayer(&img);
  ev = win->setEvent();
  for (int i = 0; i < nb_thread; i++)
    {
      th[i] = std::thread(ptr_func, ray, i, nb_thread);
    }
  while (win->isOpen() == true && exit == false)
    {
      ev->GetEvent();
      win->draw(img);
      if (ev->type == KeyPress)
	{
	  if (ev->key->code == Xt::Esc)
	    exit = true;
	}
    }
  for (int i = 0; i < nb_thread; i++)
    th[i].join();
  delete ev;
  return (0);
}
