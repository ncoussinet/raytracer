//
// calc.cpp for  in /home/coussi_n/raytracer/2new_ray/src
//
// Made by Nelson Coussinet
// Login   <coussi_n@epitech.net>
//
// Started on Mon May  5 12:43:33 2014 Nelson Coussinet
// Last update Tue May  6 11:35:00 2014 Nelson Coussinet
//

#include	<cmath>
#include	<ctgmath>
#include	"calc.hh"

double		vect_sum(t_view *vect)
{
  return ((vect->x * vect->x) + (vect->y * vect->y) + (vect->z * vect->z));
}

double		magnitude(t_view *vect)
{
  return (sqrt(vect_sum(vect)));
}

// vect unit
void		normalize(t_view *vect, t_view *new_vect)
{
  double	norm;

  norm = magnitude(vect);
  if (norm != 1)
    {
      new_vect->x = vect->x / norm;
      new_vect->y = vect->y / norm;
      new_vect->z = vect->z / norm;
    }
}

// scalar product
double		mul_vect(t_view *vect, t_view *vect2)
{
  return (vect->x * vect2->x + vect->y * vect2->y + vect->z * vect2->z);
}

void		new_cam_pos(t_pos *cam, double k, t_view *vect, t_pos *new_cam)
{
  new_cam->x = (cam->x + k * vect->x);
  new_cam->y = (cam->y + k * vect->y);
  new_cam->z = (cam->z + k * vect->z);
}

void		new_vect_sub(t_pos *pos1, t_pos *pos2, t_view *new_vect)
{
  new_vect->x = pos1->x - pos2->x;
  new_vect->y = pos1->y - pos2->y;
  new_vect->z = pos1->z - pos2->z;
}

void		check_too_strong(t_rgb *rgb)
{
  if (rgb->r > 255)
    rgb->r = 255;
  if (rgb->g > 255)
    rgb->g = 255;
  if (rgb->b > 255)
    rgb->b = 255;
}

void		new_color_div(t_rgb *rgb, int div)
{
  *rgb /= div;
  check_too_strong(rgb);
}

void		transformColorToRGB(t_rgb & new_color, unsigned int color)
{
  new_color.b = color & 0xFF;
  new_color.g = (color >> 8) & 0xFF;
  new_color.r = (color >> 16) & 0xFF;
}
