//
// raytracer.cpp for  in /home/coussi_n/raytracer
//
// Made by Nelson Coussinet
// Login   <coussi_n@epitech.net>
//
// Started on Tue Apr  1 11:42:17 2014 Nelson Coussinet
// Last update Tue May  6 16:15:44 2014 Nelson Coussinet
//

#include		<iostream>
#include		<cmath>
#include		<ctgmath>
#include		"raytracer.hh"

Raytracer::Raytracer(void) :
  path(".plan")
{
  _lenght = 500;
  _height = 500;
  this->line_nb = 0;
  this->prepareParse();
  this->openFile();
}

Raytracer::Raytracer(const std::string &scene) :
  path(scene)
{
  _lenght = 500;
  _height = 500;
  this->line_nb = 0;
  this->prepareParse();
  this->openFile();
}

Raytracer::~Raytracer(void)
{
}

IObject		*Raytracer::check_closest(t_pos &cam, t_view &vect, int numObj, t_k &res)
{
  std::list<IObject *>::iterator	it = objs.begin();
  t_k		result;
  int		check_same_obj;
  IObject	*my_obj;
  IObject	*closest;

  result.reflect = 0;
  closest = NULL;
  check_same_obj = 0;
  result.color = 0.0;
  result.k = -1.0;
  result.reflect = 0.0;
  while (it != objs.end())
    {
      my_obj = *it;
      my_obj->getKColor(vect, cam, result);
      if ((result.k < res.k || res.k < 0.0) && result.k > 0.0 && numObj != check_same_obj)
	{
	  closest = *it;
	  res.color = result.color;
	  res.k = result.k;
	}
      ++it;
      ++check_same_obj;
    }
  if (closest != NULL && closest->getReflect() > 0.0)
    this->Reflect(vect, cam, closest, res);
  return (closest);
}

void		Raytracer::initLight(t_pos &cam, t_view &my_vect, t_k &res, t_rgb &end_color)
{
  end_color.r = 0;
  end_color.g = 0;
  end_color.b = 0;
  new_cam_pos(&cam, res.k, &my_vect, &cam);
}

void		Raytracer::light(IObject *tmp_closest, t_pos &cam, t_view &vect, t_k &res)
{
  std::list<Spot2 *>::iterator	it = lum.begin();
  int		nb_spot;
  t_pos		spot_post;
  t_view	vect_n;
  double	cos_angle;
  double	brillance;
  t_rgb		color_tmp;
  t_rgb		end_color;
  int		numObj;

  cos_angle = 0;
  nb_spot = 0;
  numObj = tmp_closest->getNumObj();
  brillance = tmp_closest->getBrillance();
  color_tmp.b = res.color & 0xFF;
  color_tmp.g = (res.color & 0xFF00) >> 8;
  color_tmp.r = (res.color & 0xFF0000) >> 16;
  // color_tmp = tmp_closest->getRGB();
  this->initLight(cam, vect, res, end_color);
  tmp_closest->getNormale(cam, vect, &vect_n);
  while (it != lum.end())
    {
      spot_post = (*it)->getPosition();
      new_vect_sub(&spot_post, &cam, &vect);

      if (this->_conf._shadow && this->_conf._light)
	{
	  // this->LightAndShadow(cam, vect, numObj, res)
	  res.k = -1.0;
	  res.reflect = 0;
	  this->check_closest(cam, vect, numObj, res);
	  if (res.k < 0.0 || res.k > 1.0)
	    {
	      cos_angle = my_light.getLightColor(vect, &end_color, vect_n, &color_tmp);
	      if (true)
		my_light.getShine(brillance, &end_color, (*it)->getColor(), cos_angle);
	    }
	}

      else if (this->_conf._light && !this->_conf._shadow)
	{
	  cos_angle = my_light.getLightColor(vect, &end_color, vect_n, &color_tmp);
	  if (true)
	    my_light.getShine(brillance, &end_color, (*it)->getColor(), cos_angle);
	}

      else if (this->_conf._shadow && !this->_conf._light)
	{
	  res.k = -1.0;
	  this->check_closest(cam, vect, numObj, res);
	  if (res.k < 0.0 || res.k > 1.0)
	    {
	      t_rgb	*color_tmp;

	      color_tmp = tmp_closest->getRGB();
	      end_color += color_tmp;
	    }
	}

      else
	res.color = res.color;
      ++nb_spot;
      ++it;
    }
  if (nb_spot > 0)
    new_color_div(&end_color, nb_spot);
  res.color = end_color.r << 16 | end_color.g << 8 | end_color.b;
}

void		Raytracer::Reflect(t_view &vect, t_pos &cam, IObject *tmp_closest,
				   t_k &k)
{
  t_view	new_vect;
  t_view	new_normale;
  t_view	new_ray;
  double	scal;
  IObject	*closest;
  t_k		res;
  double	reflect;
  t_pos		new_cam;
  t_view	normale;
  t_rgb		end_color;

  closest = NULL;
  new_cam_pos(&cam, k.k, &vect, &new_cam);
  tmp_closest->getNormale(new_cam, vect, &normale);
  reflect = tmp_closest->getReflect();
  res.reflect = k.reflect + 1;
  res.k = -1.0;
  normalize(&vect, &new_vect);
  normalize(&normale, &new_normale);
  scal = mul_vect(&new_vect, &new_normale);
  new_ray.x = -2 * new_normale.x * scal;// + new_ray.x;
  new_ray.y = -2 * new_normale.y * scal;// + new_ray.y;
  new_ray.z = -2 * new_normale.z * scal;// + new_ray.z;
  if (k.reflect < 10 && tmp_closest)
    closest = check_closest(new_cam, new_ray, tmp_closest->getNumObj(), res);
  if (closest != NULL && reflect > 0.0)
    {
      transformColorToRGB(end_color, k.color);
      this->light(closest, new_cam, new_ray, res);
      end_color.b = (res.color & 0xFF) * reflect + end_color.b * (1 - reflect);
      end_color.g = ((res.color >> 8) & 0xFF) * reflect + end_color.g * (1 - reflect);
      end_color.r = ((res.color >> 16) & 0xFF) * reflect + end_color.r * (1 - reflect);
      k.color = end_color.r << 16 | end_color.g << 8 | end_color.b;
    }
}

void		Raytracer::initRay(int x, int y, t_view *my_view, t_view *vect)
{
  my_view->x = this->_lenght / (2.0 * tan(FOV * M_PI / 180.0));
  my_view->y = this->semiLenght - x;
  my_view->z = this->semiHeight - y;
  vect->x = my_view->x;
  vect->y = my_view->y;
  vect->z = my_view->z;
  // this->transRot();
}

unsigned int	Raytracer::sendRay(int x, int y)
{
  IObject	*closest;
  t_view	my_view;
  t_view	vect;
  t_pos		cam;
  t_k		res;

  closest = NULL;
  res.reflect = 0;
  res.color = 0x000000;
  res.k = -1.0;
  cam = _cam.getPosition();
  this->initRay(x, y, &my_view, &vect);
  closest = this->check_closest(cam, vect, -1, res);
  if (closest && (this->_conf._light || this->_conf._shadow))
    this->light(closest, cam, vect, res);
  if (this->_conf._cartoon)
    cartoon.tryCellShading(&(res.color));
  return (res.color);
}

int		Raytracer::calc(int xmin, int xmax)
{
  int		x;
  int		y;
  unsigned int	color;

  x = xmin;
  while (x < this->_lenght && x < xmax)
    {
      y = 0;
      while (y < this->_height)
  	{
  	  color = this->sendRay(x, y);
  	  img->pixelPut(x, y, color);
  	  ++y;
  	}
      ++x;
    }
  return (1);
}

int		Raytracer::getLenght(void)
{
  return (this->_lenght);
}

int		Raytracer::getHeight(void)
{
  return (this->_height);
}

void		Raytracer::setScene(const std::string &scenary)
{
  this->path = scenary;
}

void		Raytracer::setLayer(Xt::Layer *lay)
{
  this->img = lay;
}

//////////////////////////////////////////////////////
// Parse Begin
//////////////////////////////////////////////////////

void			Raytracer::prepareParse(void)
{
  this->ParseFunc["Window"] = &Raytracer::WinSize;
  this->ParseFunc["Camera"] = &Raytracer::CamPos;
  this->ParseFunc["Object"] = &Raytracer::ObjsCreate;
  this->ParseFunc["Spot"] = &Raytracer::SpotCreate;
  this->ParseFunc["Conf"] = &Raytracer::ConfType;
}

void			Raytracer::CamPos(std::ifstream &file)
{
  if ((this->_cam.setPosition(file, &line_nb)) == false)
    std::cerr << "error in your conf file with the Camera at line " << line_nb << std::endl;
}

void			Raytracer::ConfType(std::ifstream &file)
{
  if ((this->_conf.setConf(file, &line_nb)) == false)
    std::cerr << "error in your conf file with the Configuration at line " << line_nb << std::endl;
}

void			Raytracer::ObjsCreate(std::ifstream &file)
{
  IObject		*obj;

  if ((obj = this->Parseobj.getObj(file, &line_nb)) == NULL)
    std::cerr << "error in your conf file with your object at line " << line_nb << std::endl;
  else
    {
      objs.push_back(obj);
      obj->setNumObj(objs.size() - 1);
    }
}

void			Raytracer::SpotCreate(std::ifstream &file)
{
  Spot2			*spot;

  spot = new Spot2;
  if ((spot->setPosition(file, &line_nb)) == false)
    std::cerr << "error in your conf file with your spot at line " << line_nb << std::endl;
  else
    lum.push_back(spot);
}

void			Raytracer::WinSize(std::ifstream &file)
{
  file >> _lenght >> _height;
  this->semiLenght = _lenght / 2;
  this->semiHeight = _height / 2;
}

void			Raytracer::openFile(void)
{
  std::ifstream		file(path.c_str(), std::ios::in);

  if (file)
    {
      std::string	line;

      while (getline(file, line))
      	{
	  line_nb++;
	  if (line.compare(0, 2, "##") != 0)
	    {
	      if (this->ParseFunc[line])
		(this->*ParseFunc[line])(file);
	    }
	  else
	    std::cout << line.c_str() + 3 << std::endl;
	}
      file.close();
    }
  else
    std::cerr << "Invalid File!" << std::endl;
}

//////////////////////////////////////////////////////
// Parse End
//////////////////////////////////////////////////////
