//
// CartoonEffect.cpp for  in /home/coussi_n/raytracer/2new_ray/src
//
// Made by Nelson Coussinet
// Login   <coussi_n@epitech.net>
//
// Started on Mon May  5 15:14:00 2014 Nelson Coussinet
// Last update Mon May  5 15:15:41 2014 Nelson Coussinet
//

#include	"CartoonEffect.hh"

CartoonEffect::CartoonEffect(void)
{
}

CartoonEffect::~CartoonEffect(void)
{
}

void		CartoonEffect::tryCellShading(unsigned int *res)
{
  t_rgb		color;

  color.b = *res & 0xFF;
  color.g = (*res >> 8) & 0xFF;
  color.r = (*res >> 16) & 0xFF;

  color.b = toon(color.b, 6.0);
  color.g = toon(color.g, 6.0);
  color.r = toon(color.r, 6.0);
  *res = color.r << 16 | color.g << 8 | color.b;
}

int		CartoonEffect::toon(int c, double step)
{
  double	new_c;

  new_c = c;
  new_c /= 255.0;
  for (double i = 0.0; i <= 1.0; i += (1.0 / step))
    if (new_c < i)
      {
	new_c = (i - (1.0 / step)) * 255.0;
	c = (int)new_c;
	return (c);
      }
  return (255);
}
