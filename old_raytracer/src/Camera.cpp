//
// Camera.cpp for  in /home/coussi_n/raytracer/src
//
// Made by Nelson Coussinet
// Login   <coussi_n@epitech.net>
//
// Started on Wed Apr  2 14:13:10 2014 Nelson Coussinet
// Last update Sat Apr  5 21:20:47 2014 Nelson Coussinet
//

#include	<sstream>
#include	<iostream>
#include	"camera.hh"

Camera::Camera(void)
{
  CamPos.x = 0;
  CamPos.y = 0;
  CamPos.z = 0;
  this->ParseFunc["X = "] = &Camera::CamX;
  this->ParseFunc["Y = "] = &Camera::CamY;
  this->ParseFunc["Z = "] = &Camera::CamZ;
}

Camera::~Camera(void)
{
}

bool		Camera::setPosition(std::ifstream &file, int *line_nb)
{
  int		i;
  int		size;
  std::string	line;
  std::string	call;

  i = 0;
  while (i < 3 && getline(file, line))
    {
      (*line_nb)++;
      if (line.compare(0, 2, "##") != 0)
	{
	  if ((size = line.size()) < 5)
	    return (false);
	  call = line.substr(0, 4);
	  if (!this->ParseFunc[call])
	    return (false);
	  (this->*ParseFunc[call])(line.substr(4));
	  ++i;
	}
      else
	std::cout << line.c_str() + 3 << std::endl;
    }
  if (i < 3)
    return (false);
  return (true);
}

const t_pos	&Camera::getPosition(void) const
{
  return (this->CamPos);
}

void		Camera::CamX(const std::string &line)
{
  std::istringstream(line) >> this->CamPos.x;
}

void		Camera::CamY(const std::string &line)
{
  std::istringstream(line) >> this->CamPos.y;
}

void		Camera::CamZ(const std::string &line)
{
  std::istringstream(line) >> this->CamPos.z;
}
