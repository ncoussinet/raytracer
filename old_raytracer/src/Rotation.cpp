//
// Rotation.cpp for  in /home/coussi_n/raytracer/src/objects
//
// Made by Nelson Coussinet
// Login   <coussi_n@epitech.net>
//
// Started on Thu Apr  3 17:21:48 2014 Nelson Coussinet
// Last update Fri Apr 25 18:08:19 2014 Nelson Coussinet
//

#include	<cmath>
#include	"Rotation.hh"

Rotation::Rotation(void)
{
}

Rotation::~Rotation(void)
{
}

void		Rotation::rotate_z(double matrix[3][3], t_view *pos, double angle)
{
  double	rcos;
  double	rsin;

  while (angle >= 360.0 || angle <= -360.0)
    {
      if (angle >= 360.0)
	angle -= 360.0;
      else
	angle += 360.0;
    }
  angle = (angle * M_PI) / 180.0;
  rcos = cos(angle);
  rsin = sin(angle);
  matrix[0][0] = rcos;
  matrix[1][0] = -rsin;
  matrix[2][0] = 0;
  matrix[0][1] = rsin;
  matrix[1][1] = rcos;
  matrix[2][1] = 0;
  matrix[0][2] = 0;
  matrix[1][2] = 0;
  matrix[2][2] = 1;
  mul_matrix(matrix, pos);
}

void		Rotation::rotate_y(double matrix[3][3], t_view *pos, double angle)
{
  double	rcos;
  double	rsin;

  while (angle >= 360.0 || angle <= -360.0)
    {
      if (angle >= 360.0)
	angle -= 360.0;
      else
	angle += 360.0;
    }
  angle = (angle * M_PI) / 180.0;
  rcos = cos(angle);
  rsin = sin(angle);
  matrix[0][0] = rcos;
  matrix[1][0] = 0;
  matrix[2][0] = rsin;
  matrix[0][1] = 0;
  matrix[1][1] = 1;
  matrix[2][1] = 0;
  matrix[0][2] = -rsin;
  matrix[1][2] = 0;
  matrix[2][2] = rcos;
  mul_matrix(matrix, pos);
}

void		Rotation::rotate_x(double matrix[3][3], t_view *pos, double angle)
{
  double	rcos;
  double	rsin;

  while (angle >= 360.0 || angle <= -360.0)
    {
      if (angle >= 360.0)
	angle -= 360.0;
      else
	angle += 360.0;
    }
  angle = (angle * M_PI) / 180.0;
  rcos = cos(angle);
  rsin = sin(angle);
  matrix[0][0] = 1;
  matrix[1][0] = 0;
  matrix[2][0] = 0;
  matrix[0][1] = 0;
  matrix[1][1] = rcos;
  matrix[2][1] = -rsin;
  matrix[0][2] = 0;
  matrix[1][2] = rsin;
  matrix[2][2] = rcos;
  mul_matrix(matrix, pos);
}

void		Rotation::mul_matrix(double matrix[3][3], t_view *pos)
{
  t_view	res;

  res.x = (pos->x * matrix[0][0]
	   + pos->y * matrix[1][0]
	   + pos->z * matrix[2][0]);
  res.y = (pos->x * matrix[0][1]
	   + pos->y * matrix[1][1]
	   + pos->z * matrix[2][1]);
  res.z = (pos->x * matrix[0][2]
	   + pos->y * matrix[1][2]
	   + pos->z * matrix[2][2]);
  pos->x = res.x;
  pos->y = res.y;
  pos->z = res.z;
}

void		Rotation::rotate(t_view *pos, double x, double y, double z)
{
  double	matrix[3][3];

  rotate_z(matrix, pos, -z);
  rotate_y(matrix, pos, -y);
  rotate_x(matrix, pos, -x);
}
