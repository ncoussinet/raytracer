//
// Cylinder.cpp for  in /home/coussi_n/raytracer/src/objects
//
// Made by Nelson Coussinet
// Login   <coussi_n@epitech.net>
//
// Started on Thu Apr  3 19:38:28 2014 Nelson Coussinet
// Last update Tue May  6 12:37:06 2014 Nelson Coussinet
//
//

#include	<cmath>
#include	<iostream>
#include	"Cylinder.hh"

Cylinder::Cylinder(void)
{
  this->type = CYLINDER;
  this->initParse();
  this->rgb = new t_rgb;
  pos.x = 0;
  pos.y = 0;
  pos.z = 0;
  ray = 10;
  rgb->r = 0;
  rgb->g = 0;
  rgb->b = 0;
  rot.x = 0.0;
  rot.y = 0.0;
  rot.z = 0.0;
  brill = 0.0;
  reflect = 0.0;
}

void		Cylinder::initParse(void)
{
  this->ParseFunc["X = "] = &Cylinder::setX;
  this->ParseFunc["Y = "] = &Cylinder::setY;
  this->ParseFunc["Z = "] = &Cylinder::setZ;
  this->ParseFunc["R = "] = &Cylinder::setRay;
  this->ParseFunc["RX = "] = &Cylinder::setRotX;
  this->ParseFunc["RY = "] = &Cylinder::setRotY;
  this->ParseFunc["RZ = "] = &Cylinder::setRotZ;
  this->ParseFunc["RGB = "] = &Cylinder::setRGB;
  this->ParseFunc["BRI = "] = &Cylinder::setBrillance;
  this->ParseFunc["REFLECT = "] = &Cylinder::setReflect;
}

void		Cylinder::setReflect(const std::string &line)
{
  std::istringstream(line) >> this->reflect;
  this->reflect /= 100.0;
}

void		Cylinder::setBrillance(const std::string &line)
{
  std::istringstream(line) >> this->brill;
  this->brill /= 100.0;
}

void		Cylinder::setRotX(const std::string &line)
{
  std::istringstream(line) >> this->rot.x;
}

void		Cylinder::setRotY(const std::string &line)
{
  std::istringstream(line) >> this->rot.x;
}

void		Cylinder::setRotZ(const std::string &line)
{
  std::istringstream(line) >> this->rot.x;
}

void		Cylinder::setX(const std::string &line)
{
  std::istringstream(line) >> this->pos.x;
}

void		Cylinder::setY(const std::string &line)
{
  std::istringstream(line) >> this->pos.y;
}

void		Cylinder::setZ(const std::string &line)
{
  std::istringstream(line) >> this->pos.z;
}

void		Cylinder::setRay(const std::string &line)
{
  std::istringstream(line) >> this->ray;
}

void		Cylinder::setRGB(const std::string &line)
{
  std::istringstream(line) >> rgb->r >> rgb->g >> rgb->b;
}

double		Cylinder::getReflect(void)
{
  return (this->reflect);
}

Type		Cylinder::getType(void) const
{
  return (this->type);
}

double		Cylinder::getBrillance(void)
{
  return (this->brill);
}

int		Cylinder::getNumObj(void)
{
  return (numb_obj);
}

void		Cylinder::setNumObj(int nb)
{
  numb_obj = nb;
}

void		Cylinder::getNormale(t_pos &view, t_view &vect, t_view *new_vect)
{
  (void)vect;
  new_vect->x = view.x - this->pos.x;
  new_vect->y = view.y - this->pos.y;
  new_vect->z = 0;
  rotation.rotate(new_vect, rot.x, rot.y, rot.z);
}

t_rgb		*Cylinder::getRGB(void)
{
  return (this->rgb);
}

double		Cylinder::calcDelta(double a, double b, double delta)
{
  double	res;
  double	res2;

  res = (-b - sqrt(delta)) / (2 * a);
  if (delta > 0.0)
    {

      res2 = (-b + sqrt(delta)) / (2 * a);
      if (res2 < res && res2 > 0.0)
	return (res2);
    }
  return (res);
}

void		Cylinder::getKColor(t_view &vect, t_pos &cam, t_k &res)
{
  t_view	tmp_view;
  t_view	tmp_vect;
  double	a;
  double	b;
  double	c;
  double	delta;

  res.k = -1;
  res.color = rgb->r << 16 | rgb->g << 8 | rgb->b;
  tmp_view.x = (double)cam.x - (double)pos.x;
  tmp_view.y = (double)cam.y - (double)pos.y;
  tmp_view.z = (double)cam.z - (double)pos.z;
  tmp_vect.x = vect.x;
  tmp_vect.y = vect.y;
  tmp_vect.z = vect.z;
  rotation.rotate(&tmp_view, rot.x, rot.y, rot.z);
  rotation.rotate(&tmp_vect, rot.x, rot.y, rot.z);
  a = (tmp_vect.x * tmp_vect.x)
    + (tmp_vect.y * tmp_vect.y);
  b = 2 * (tmp_view.x * tmp_vect.x
	   + tmp_view.y * tmp_vect.y);
  c = tmp_view.x * tmp_view.x +
    tmp_view.y * tmp_view.y -
    ray * ray;
  delta = b * b - 4 * a * c;
  if (delta >= 0.0)
    res.k = calcDelta(a, b, delta);
}

bool		Cylinder::setCylinder(std::ifstream &file, int *line_nb)
{
  int		i;
  int		size;
  std::string	line;
  std::string	call;

  i = 0;
  while (i < ELEM_MAX_CYLINDER && getline(file, line))
    {
      (*line_nb)++;
      if (i >= ELEM_MIN_CYLINDER && !line.compare("END"))
	return (true);
      if (line.compare(0, 2, "##") != 0)
	{
	  if (line.size() < 5)
	    return (false);
	  if ((size = line.find(" = ") + 3) == std::string::npos)
	    return (false);
	  call = line.substr(0, size);
	  if (!this->ParseFunc[call])
	    return (false);
	  (this->*ParseFunc[call])(line.substr(size));
	  ++i;
	}
      else
	std::cout << line.c_str() + 3 << std::endl;
    }
  return (false);
}
