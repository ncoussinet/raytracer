//
// ObjectParse.cpp for  in /home/coussi_n/raytracer/src
//
// Made by Nelson Coussinet
// Login   <coussi_n@epitech.net>
//
// Started on Wed Apr  2 15:36:37 2014 Nelson Coussinet
// Last update Wed Apr 16 19:03:26 2014 Nelson Coussinet
//

#include	<iostream>
#include	"ObjectParse.hh"

ObjectParse::ObjectParse(void)
{
  this->ParseFunc["CIRCLE"] = &ObjectParse::my_Circle;
  this->ParseFunc["CYLINDER"] = &ObjectParse::my_Cylinder;
  this->ParseFunc["CONE"] = &ObjectParse::my_Cone;
  this->ParseFunc["PLAN"] = &ObjectParse::my_Plan;
  this->ParseFunc["CUBETROUE"] = &ObjectParse::my_CubeTroue;
}

ObjectParse::~ObjectParse(void)
{
}

IObject		*ObjectParse::my_Circle(std::ifstream &file, int *nb_line)
{
  Circle	*my_obj;

  my_obj = new Circle;
  if ((my_obj->setCircle(file, nb_line)) == false)
    return (NULL);
  return (my_obj);
}

IObject		*ObjectParse::my_CubeTroue(std::ifstream &file, int *nb_line)
{
  CubeTroue	*my_obj;

  my_obj = new CubeTroue;
  if ((my_obj->setCubeTroue(file, nb_line)) == false)
    return (NULL);
  return (my_obj);
}

IObject		*ObjectParse::my_Cylinder(std::ifstream &file, int *nb_line)
{
  Cylinder	*my_obj;

  my_obj = new Cylinder;
  if ((my_obj->setCylinder(file, nb_line)) == false)
    return (NULL);
  return (my_obj);
}

IObject		*ObjectParse::my_Cone(std::ifstream &file, int *nb_line)
{
  Cone		*my_obj;

  my_obj = new Cone;
  if ((my_obj->setCone(file, nb_line)) == false)
    return (NULL);
  return (my_obj);
}

IObject		*ObjectParse::my_Plan(std::ifstream &file, int *nb_line)
{
  Plan		*my_obj;

  my_obj = new Plan;
  if ((my_obj->setPlan(file, nb_line)) == false)
    return (NULL);
  return (my_obj);
}

IObject		*ObjectParse::getObj(std::ifstream &file, int *nb_line)
{
  int		i;
  std::string	line;

  i = 0;
  while (i < 1 && getline(file, line))
    {
      (*nb_line)++;
      if (line.compare(0, 2, "##") != 0)
	{
	  if (!this->ParseFunc[line])
	    return (NULL);
	  return ((this->*ParseFunc[line])(file, nb_line));
	  ++i;
	}
      else
	std::cout << line.c_str() + 3 << std::endl;
    }
  return (NULL);
}
