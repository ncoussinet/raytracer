//
// Cone.cpp for  in /home/coussi_n/raytracer/src/objects
//
// Made by Nelson Coussinet
// Login   <coussi_n@epitech.net>
//
// Started on Thu Apr  3 19:38:28 2014 Nelson Coussinet
// Last update Tue May  6 12:13:12 2014 Nelson Coussinet
//
//

#include	<cmath>
#include	<iostream>
#include	"Cone.hh"

Cone::Cone(void)
{
  this->type = CONE;
  this->initParse();
  this->rgb = new t_rgb;
  pos.x = 0;
  pos.y = 0;
  pos.z = 0;
  cst = 10;
  rgb->r = 0;
  rgb->g = 0;
  rgb->b = 0;
  rot.x = 0.0;
  rot.y = 0.0;
  rot.z = 0.0;
  brill = 0.0;
  reflect = 0.0;
  }

void		Cone::initParse(void)
{
  this->ParseFunc["X = "] = &Cone::setX;
  this->ParseFunc["Y = "] = &Cone::setY;
  this->ParseFunc["Z = "] = &Cone::setZ;
  this->ParseFunc["CST = "] = &Cone::setCst;
  this->ParseFunc["RX = "] = &Cone::setRotX;
  this->ParseFunc["RY = "] = &Cone::setRotY;
  this->ParseFunc["RZ = "] = &Cone::setRotZ;
  this->ParseFunc["RGB = "] = &Cone::setRGB;
  this->ParseFunc["BRI = "] = &Cone::setBrillance;
  this->ParseFunc["REFLECT = "] = &Cone::setReflect;
}

void		Cone::setReflect(const std::string &line)
{
  std::istringstream(line) >> this->reflect;
  this->reflect /= 100.0;
}

void		Cone::setBrillance(const std::string &line)
{
  std::istringstream(line)>> this->brill;
  this->brill /= 100.0;
}

void		Cone::setRotX(const std::string &line)
{
  std::istringstream(line) >> this->rot.x;
}

void		Cone::setRotY(const std::string &line)
{
  std::istringstream(line) >> this->rot.y;
}

void		Cone::setRotZ(const std::string &line)
{
  std::istringstream(line) >> this->rot.z;
}

void		Cone::setX(const std::string &line)
{
  std::istringstream(line) >> this->pos.x;
}

void		Cone::setY(const std::string &line)
{
  std::istringstream(line) >> this->pos.y;
}

void		Cone::setZ(const std::string &line)
{
  std::istringstream(line) >> this->pos.z;
}

void		Cone::setCst(const std::string &line)
{
  std::istringstream(line) >> this->cst;
  tan_angle = tan((M_PI * cst) / 180) * tan((M_PI * cst) / 180);
}

void		Cone::setRGB(const std::string &line)
{
  std::istringstream(line) >> rgb->r >> rgb->g >> rgb->b;
}

double		Cone::getReflect(void)
{
  return (this->reflect);
}

int		Cone::getNumObj(void)
{
  return (numb_obj);
}

double		Cone::getBrillance(void)
{
  return (this->brill);
}

void		Cone::setNumObj(int nb)
{
  numb_obj = nb;
}

Type		Cone::getType(void) const
{
  return (this->type);
}

double		Cone::calcDelta(double a, double b, double delta)
{
  double	res;

  res = (-b - sqrt(delta)) / (2 * a);
  if (delta > 0.0)
    {
      double	res2;

      res2 = (-b + sqrt(delta)) / (2 * a);
      if (res2 < res && res2 > 0.0)
	return (res2);
    }
  return (res);
}

void		Cone::getNormale(t_pos &view, t_view &vect, t_view *new_vec)
{
  (void)vect;
  new_vec->x = view.x - pos.x;
  new_vec->y = view.y - pos.y;
  new_vec->z = -tan_angle * view.z / 2.0 - pos.z;
  rotation.rotate(new_vec, rot.x, rot.y, rot.z);
}

void		Cone::setNormale(double k, t_view &vect, t_pos &cam)
{
  (void)vect;
  (void)cam;
  (void)k;
}

t_rgb		*Cone::getRGB(void)
{
  return (this->rgb);
}

void		Cone::getKColor(t_view &vect, t_pos &cam, t_k &res)
{
  t_view	tmp_view;
  t_view	tmp_vect;
  double	a;
  double	b;
  double	c;
  double	delta;

  res.k = -1;
  tmp_view.x = (double)cam.x - (double)pos.x;
  tmp_view.y = (double)cam.y - (double)pos.y;
  tmp_view.z = (double)cam.z - (double)pos.z;
  tmp_vect.x = vect.x;
  tmp_vect.y = vect.y;
  tmp_vect.z = vect.z;
  rotation.rotate(&tmp_view, rot.x, rot.y, rot.z);
  rotation.rotate(&tmp_vect, rot.x, rot.y, rot.z);
  a = (tmp_vect.x * tmp_vect.x)
    + (tmp_vect.y * tmp_vect.y)
    - (tan_angle  * tmp_vect.z * tmp_vect.z);
  b = 2 * (tmp_vect.x * tmp_view.x
	   + tmp_view.y * tmp_vect.y
	   - tan_angle * tmp_vect.z * tmp_view.z);
  c = tmp_view.x * tmp_view.x +
    tmp_view.y * tmp_view.y
    - tan_angle * tmp_view.z * tmp_view.z;
  delta = b * b - 4 * a * c;
  if (delta >= 0.0)
    {
      res.color = rgb->r << 16 | rgb->g << 8 | rgb->b;
      res.k = calcDelta(a, b, delta);
      this->setNormale(res.k, vect, cam);
    }
}

bool		Cone::setCone(std::ifstream &file, int *line_nb)
{
  int		i;
  int		size;
  std::string	line;
  std::string	call;

  i = 0;
  while (i < ELEM_MAX_CONE && getline(file, line))
    {
      (*line_nb)++;
      if (i >= ELEM_MIN_CONE && !line.compare("END"))
	return (true);
      if (line.compare(0, 2, "##") != 0)
	{
	  if (line.size() < 5)
	    return (false);
	  if ((size = line.find(" = ") + 3) == std::string::npos)
	    return (false);
	  call = line.substr(0, size);
	  if (!this->ParseFunc[call])
	    return (false);
	  (this->*ParseFunc[call])(line.substr(size));
	  ++i;
	}
      else
	std::cout << line.c_str() + 3 << std::endl;
    }
  return (false);
}
