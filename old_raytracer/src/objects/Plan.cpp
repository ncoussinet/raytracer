//
// Plan.cpp for  in /home/coussi_n/raytracer/src
//
// Made by Nelson Coussinet
// Login   <coussi_n@epitech.net>
//
// Started on Wed Apr  2 19:01:10 2014 Nelson Coussinet
// Last update Tue May  6 12:12:37 2014 Nelson Coussinet
//

//

#include	<iostream>
#include	"Plan.hh"

Plan::Plan(void)
{
  this->type = PLAN;
  this->initParse();
  this->rgb = new t_rgb;
  pos.x = 0;
  pos.y = 0;
  pos.z = 0;
  rgb->r = 0;
  rgb->g = 0;
  rgb->b = 0;
  rot.x = 0;
  rot.y = 0;
  rot.z = 0;
  brill = 0.0;
  reflect = 0.0;
}

void		Plan::setNumObj(int nb)
{
  numb_obj = nb;
}

int		Plan::getNumObj(void)
{
  return (numb_obj);
}

void		Plan::getKColor(t_view &vect, t_pos &cam, t_k &res)
{
  t_view	tmp_view;
  t_view	tmp_vect;

  res.k = -1;
  res.color = rgb->r << 16 | rgb->g << 8 | rgb->b;
  tmp_view.x = cam.x - pos.x;
  tmp_view.y = cam.y - pos.y;
  tmp_view.z = cam.z - pos.z;
  tmp_vect.x = vect.x;
  tmp_vect.y = vect.y;
  tmp_vect.z = vect.z;
  rotation.rotate(&tmp_view, rot.x, rot.y, rot.z);
  rotation.rotate(&tmp_vect, rot.x, rot.y, rot.z);
  if (tmp_vect.z != 0.0)
    res.k = -(tmp_view.z / tmp_vect.z);
}

void		Plan::initParse(void)
{
  this->ParseFunc["RX = "] = &Plan::setRotX;
  this->ParseFunc["RY = "] = &Plan::setRotY;
  this->ParseFunc["RZ = "] = &Plan::setRotZ;
  this->ParseFunc["X = "] = &Plan::setX;
  this->ParseFunc["Y = "] = &Plan::setY;
  this->ParseFunc["Z = "] = &Plan::setZ;
  this->ParseFunc["RGB = "] = &Plan::setRGB;
  this->ParseFunc["BRI = "] = &Plan::setBrillance;
  this->ParseFunc["REFLECT = "] = &Plan::setReflect;
}

void		Plan::setReflect(const std::string &line)
{
  std::istringstream(line) >> this->reflect;
  this->reflect /= 100.0;
}

void		Plan::setBrillance(const std::string &line)
{
  std::istringstream(line) >> this->brill;
  this->brill /= 100.0;
}

void		Plan::setRotX(const std::string &line)
{
  std::istringstream(line) >> this->rot.x;
}

void		Plan::setRotY(const std::string &line)
{
  std::istringstream(line) >> this->rot.y;
}

void		Plan::setRotZ(const std::string &line)
{
  std::istringstream(line) >> this->rot.z;
}

void		Plan::setZ(const std::string &line)
{
  std::istringstream(line) >> this->pos.z;
}

void		Plan::setY(const std::string &line)
{
  std::istringstream(line) >> this->pos.y;
}

void		Plan::setX(const std::string &line)
{
  std::istringstream(line) >> this->pos.x;
}

void		Plan::setRGB(const std::string &line)
{
  std::istringstream(line) >> rgb->r >> rgb->g >> rgb->b;
}

double		Plan::getReflect(void)
{
  return (this->reflect);
}

double		Plan::getBrillance(void)
{
  return (this->brill);
}

void		Plan::getNormale(t_pos &view, t_view &vect, t_view *new_vect)
{
  // double	scalaire;

  (void)view;
  (void)vect;
  new_vect->x = 0;
  new_vect->y = 0;
  new_vect->z = 0 - pos.z;
  // rotation.rotate(new_vect, rot.x, rot.y, rot.z);
  // scalaire = new_vect->x * vect.x + new_vect->y * vect.y + new_vect->z * vect.z;
  // new_vect->x = (scalaire <= 0 ? new_vect->x : -new_vect->x);
  // new_vect->y = (scalaire <= 0 ? new_vect->y : -new_vect->y);
  // new_vect->z = (scalaire <= 0 ? new_vect->z : -new_vect->z);
}

t_rgb		*Plan::getRGB(void)
{
  return (this->rgb);
}

Type		Plan::getType(void) const
{
  return (this->type);
}

bool		Plan::setPlan(std::ifstream &file, int *line_nb)
{
  int		i;
  int		size;
  std::string	line;
  std::string	call;

  i = 0;
  while (i < ELEM_MAX_PLAN && getline(file, line))
    {
      (*line_nb)++;
      if (i >= ELEM_MIN_PLAN && !line.compare("END"))
	return (true);
      if (line.compare(0, 2, "##") != 0)
	{
	  if (line.size() < 5)
	    return (false);
	  if ((size = line.find(" = ") + 3) == std::string::npos)
	    return (false);
	  call = line.substr(0, size);
	  if (!this->ParseFunc[call])
	    return (false);
	  (this->*ParseFunc[call])(line.substr(size));
	  ++i;
	}
      else
	std::cout << line.c_str() + 3 << std::endl;
    }
  if (i < ELEM_MIN_PLAN)
    return (false);
  return (true);
}
