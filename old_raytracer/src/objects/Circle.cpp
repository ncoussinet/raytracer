//
// Circle.cpp for  in /home/coussi_n/raytracer/src
//
// Made by Nelson Coussinet
// Login   <coussi_n@epitech.net>
//
// Started on Wed Apr  2 16:23:46 2014 Nelson Coussinet
// Last update Tue May  6 12:11:06 2014 Nelson Coussinet
//

#include	<cmath>
#include	<iostream>
#include	"Circle.hh"

Circle::Circle(void)
{
  this->type = CIRCLE;
  this->initParse();
  this->rgb = new t_rgb;
  pos.x = 0;
  pos.y = 0;
  pos.z = 0;
  ray = 10;
  rgb->r = 0;
  rgb->g = 0;
  rgb->b = 0;
  rot.x = 0.0;
  rot.y = 0.0;
  rot.z = 0.0;
  brill = 0.0;
  reflect = 0.0;
}

void		Circle::initParse(void)
{
  this->ParseFunc["X = "] = &Circle::setX;
  this->ParseFunc["Y = "] = &Circle::setY;
  this->ParseFunc["Z = "] = &Circle::setZ;
  this->ParseFunc["R = "] = &Circle::setRay;
  this->ParseFunc["RX = "] = &Circle::setRotX;
  this->ParseFunc["RY = "] = &Circle::setRotY;
  this->ParseFunc["RZ = "] = &Circle::setRotZ;
  this->ParseFunc["RGB = "] = &Circle::setRGB;
  this->ParseFunc["BRI = "] = &Circle::setBrillance;
  this->ParseFunc["REFLECT = "] = &Circle::setReflect;
}

void		Circle::setReflect(const std::string &line)
{
  std::istringstream(line) >> this->reflect;
  this->reflect /= 100.0;
}

void		Circle::setBrillance(const std::string &line)
{
  std::istringstream(line) >> this->brill;
  this->brill /= 100.0;
}

void		Circle::setRotX(const std::string &line)
{
  std::istringstream(line) >> this->rot.x;
}

void		Circle::setRotY(const std::string &line)
{
  std::istringstream(line) >> this->rot.x;
}

void		Circle::setRotZ(const std::string &line)
{
  std::istringstream(line) >> this->rot.x;
}

void		Circle::setX(const std::string &line)
{
  std::istringstream(line) >> this->pos.x;
}

void		Circle::setY(const std::string &line)
{
  std::istringstream(line) >> this->pos.y;
}

void		Circle::setZ(const std::string &line)
{
  std::istringstream(line) >> this->pos.z;
}

void		Circle::setRay(const std::string &line)
{
  std::istringstream(line) >> this->ray;
}

void		Circle::setRGB(const std::string &line)
{
  std::istringstream(line) >> rgb->r >> rgb->g >> rgb->b;
}

void		Circle::setNumObj(int nb)
{
  numb_obj = nb;
}

double		Circle::getReflect(void)
{
  return (this->reflect);
}

double		Circle::getBrillance(void)
{
  return (this->brill);
}

int		Circle::getNumObj(void)
{
  return (numb_obj);
}

t_rgb		*Circle::getRGB(void)
{
  return (this->rgb);
}

Type		Circle::getType(void) const
{
  return (this->type);
}

double		Circle::calcDelta(double a, double b, double delta)
{
  double	res;

  res = (-b - sqrt(delta)) / (2.0 * a);
  if (delta > 0.0)
    {
      double	res2;

      res2 = (-b + sqrt(delta)) / (2.0 * a);
      if (res2 < res && res2 > 0.0)
	return (res2);
    }
  return (res);
}

void		Circle::getNormale(t_pos &view, t_view &vect, t_view *new_vect)
{
  (void)vect;
  new_vect->x = view.x - this->pos.x;
  new_vect->y = view.y - this->pos.y;
  new_vect->z = view.z - this->pos.z;
}

void		Circle::getKColor(t_view &vect, t_pos &cam, t_k &res)
{
  t_view	tmp_view;
  t_view	tmp_vect;
  double	a;
  double	b;
  double	c;
  double	delta;

  delta = -1.0;
  res.k = -1.0;
  res.color = rgb->r << 16 | rgb->g << 8 | rgb->b;
  tmp_view.x = cam.x - pos.x;
  tmp_view.y = cam.y - pos.y;
  tmp_view.z = cam.z - pos.z;
  rotation.rotate(&tmp_view, rot.x, rot.y, rot.z);
  tmp_vect.x = vect.x;
  tmp_vect.y = vect.y;
  tmp_vect.z = vect.z;
  a = (tmp_vect.x * tmp_vect.x)
    + (tmp_vect.y * tmp_vect.y)
    + (tmp_vect.z * tmp_vect.z);
  b = 2 * (tmp_view.x * tmp_vect.x
	   + tmp_view.y * tmp_vect.y
	   + tmp_view.z * tmp_vect.z);
  c = tmp_view.x * tmp_view.x +
    tmp_view.y * tmp_view.y +
    tmp_view.z * tmp_view.z -
    ray * ray;
  delta = b * b - 4 * a * c;
  if (delta >= 0.0)
    res.k = calcDelta(a, b, delta);
}

bool		Circle::setCircle(std::ifstream &file, int *line_nb)
{
  int		i;
  int		size;
  std::string	line;
  std::string	call;

  i = 0;
  while (i < ELEM_MAX_CIRCLE && getline(file, line))
    {
      (*line_nb)++;
      if (i >= ELEM_MIN_CIRCLE && !line.compare("END"))
	return (true);
      if (line.compare(0, 2, "##") != 0)
	{
	  if (line.size() < 5)
	    return (false);
	  if ((size = line.find(" = ") + 3) == std::string::npos)
	    return (false);
	  call = line.substr(0, size);
	  if (!this->ParseFunc[call])
	    return (false);
	  (this->*ParseFunc[call])(line.substr(size));
	  ++i;
	}
      else
	std::cout << line.c_str() + 3 << std::endl;
    }
  return (false);
}
