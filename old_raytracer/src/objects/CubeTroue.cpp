//
// CubeTroue.cpp for  in /home/coussi_n/raytracer/src/objects
//
// Made by Nelson Coussinet
// Login   <coussi_n@epitech.net>
//
// Started on Wed Apr 16 16:30:40 2014 Nelson Coussinet
// Last update Tue May  6 12:14:04 2014 Nelson Coussinet
//

#include	<cmath>
#include	<iostream>
#include	"CubeTroue.hh"

CubeTroue::CubeTroue(void)
{
  this->type = CUBETROUE;
  this->initParse();
  this->rgb = new t_rgb;
  pos.x = 0;
  pos.y = 0;
  pos.z = 0;
  rgb->r = 0;
  rgb->g = 0;
  rgb->b = 0;
  rot.x = 0.0;
  rot.y = 0.0;
  rot.z = 0.0;
  reflect = 0.0;
}

void		CubeTroue::initParse(void)
{
  this->ParseFunc["X = "] = &CubeTroue::setX;
  this->ParseFunc["Y = "] = &CubeTroue::setY;
  this->ParseFunc["Z = "] = &CubeTroue::setZ;
  this->ParseFunc["RX = "] = &CubeTroue::setRotX;
  this->ParseFunc["RY = "] = &CubeTroue::setRotY;
  this->ParseFunc["RZ = "] = &CubeTroue::setRotZ;
  this->ParseFunc["RGB = "] = &CubeTroue::setRGB;
  // this->ParseFunc["BRI = "] = &CubeTroue::setBrillance;
  this->ParseFunc["REFLECT = "] = &CubeTroue::setReflect;
}

void		CubeTroue::setReflect(const std::string &line)
{
  std::istringstream(line) >> this->reflect;
  this->reflect /= 100.0;
}

void		CubeTroue::setRotX(const std::string &line)
{
  std::istringstream(line) >> this->rot.x;
}

void		CubeTroue::setRotY(const std::string &line)
{
  std::istringstream(line) >> this->rot.x;
}

void		CubeTroue::setRotZ(const std::string &line)
{
  std::istringstream(line) >> this->rot.x;
}

void		CubeTroue::setX(const std::string &line)
{
  std::istringstream(line) >> this->pos.x;
}

void		CubeTroue::setY(const std::string &line)
{
  std::istringstream(line) >> this->pos.y;
}

void		CubeTroue::setZ(const std::string &line)
{
  std::istringstream(line) >> this->pos.z;
}

void		CubeTroue::setRGB(const std::string &line)
{
  std::istringstream(line) >> rgb->r >> rgb->g >> rgb->b;
}

double		CubeTroue::getReflect(void)
{
  return (reflect);
}

Type		CubeTroue::getType(void) const
{
  return (this->type);
}

int		CubeTroue::getNumObj(void)
{
  return (numb_obj);
}

void		CubeTroue::setNumObj(int nb)
{
  numb_obj = nb;
}

double		CubeTroue::getBrillance(void)
{
  return (0.0);
}

void		CubeTroue::getNormale(t_pos &view, t_view &vect, t_view *new_vect)
{
  // pas encore fait
  (void)vect;
  new_vect->x = view.x - this->pos.x;
  new_vect->y = view.y - this->pos.y;
  new_vect->z = 0;
  rotation.rotate(new_vect, rot.x, rot.y, rot.z);
}

t_rgb		*CubeTroue::getRGB(void)
{
  return (this->rgb);
}

double		CubeTroue::calcDelta(double a, double b, double delta)
{
  double	res;

  res = (-b - sqrt(delta)) / (2 * a);
  if (delta > 0.0)
    {
      double	res2;

      res2 = (-b + sqrt(delta)) / (2 * a);
      if (res2 < res && res2 > 0.0)
	return (res2);
    }
  return (res);
}

void		CubeTroue::check_troue(t_view &vect, t_pos &cam, double *res)
{
  t_view	tmp_view;
  t_view	tmp_vect;
  double	a;
  double	b;
  double	c;
  double	delta;

  *res = -1.0;
  tmp_view.x = cam.x - pos.x;
  tmp_view.y = cam.y - pos.y;
  tmp_view.z = cam.z - pos.z;
  rotation.rotate(&tmp_view, rot.x, rot.y, rot.z);
  tmp_vect.x = vect.x;
  tmp_vect.y = vect.y;
  tmp_vect.z = vect.z;
  a = (tmp_vect.x * tmp_vect.x)
    + (tmp_vect.y * tmp_vect.y)
    + (tmp_vect.z * tmp_vect.z);
  b = 2 * (tmp_view.x * tmp_vect.x
	   + tmp_view.y * tmp_vect.y
	   + tmp_view.z * tmp_vect.z);
  c = tmp_view.x * tmp_view.x +
    tmp_view.y * tmp_view.y +
    tmp_view.z * tmp_view.z -
    3.0 * 3.0;
  delta = b * b - 4 * a * c;
  if (delta >= 0.0)
    *res = calcDelta(a, b, delta);
}

void		CubeTroue::getKColor(t_view &vect, t_pos &cam, t_k &res)
{
  t_view	tmp_view;
  t_view	tmp_vect;
  double	tab[5];
  int		nbroots = 0;
  double	roots[4] = {0.0, 0.0, 0.0, 0.0};

  res.k = -1;
  res.color = rgb->r << 16 | rgb->g << 8 | rgb->b;
  tmp_view.x = (double)cam.x - (double)pos.x;
  tmp_view.y = (double)cam.y - (double)pos.y;
  tmp_view.z = (double)cam.z - (double)pos.z;
  tmp_vect.x = vect.x;
  tmp_vect.y = vect.y;
  tmp_vect.z = vect.z;
  rotation.rotate(&tmp_view, rot.x, rot.y, rot.z);
  rotation.rotate(&tmp_vect, rot.x, rot.y, rot.z);
  // travailler ici
  tab[0] = pow(tmp_vect.x, 4) + pow(tmp_vect.y, 4) + pow(tmp_vect.z, 4);
  tab[1] = 4 * (pow(tmp_vect.x, 3) * tmp_view.x + pow(tmp_vect.y, 3) * tmp_view.y + pow(tmp_vect.z, 3) * tmp_view.z);
  tab[2] = 6 * (tmp_vect.x * tmp_vect.x * tmp_view.x * tmp_view.x
		+ tmp_vect.y * tmp_vect.y * tmp_view.y * tmp_view.y
		+ tmp_vect.z * tmp_vect.z * tmp_view.z * tmp_view.z) -
    5 * (tmp_vect.x * tmp_vect.x + tmp_vect.y * tmp_vect.y + tmp_vect.z * tmp_vect.z);
  tab[3] = 4 * (tmp_vect.x * pow(tmp_view.x, 3) + tmp_vect.y * pow(tmp_view.y, 3) + tmp_vect.z * pow(tmp_view.z, 3)) -
    10 * (tmp_view.x * tmp_vect.x + tmp_view.y * tmp_vect.y + tmp_view.z * tmp_vect.z);
  tab[4] = pow(tmp_view.x, 4) + pow(tmp_view.y, 4) + pow(tmp_view.z, 4) -
    5 * (tmp_view.x * tmp_view.x + tmp_view.y * tmp_view.y + tmp_view.z * tmp_view.z);

  Polynomial	poly(tab[0], tab[1], tab[2], tab[3], tab[4]);

  nbroots = poly.SolveQuartic(roots);
  if (!nbroots)
    return ;
  else
    {
      for (int i = 0; i < nbroots; i++)
	if (roots[i] > 0.001 && (roots[i] < res.k || res.k < 0.0))
	  res.k = roots[i];
      double my_res;
      check_troue(vect, cam, &my_res);
      if (my_res < res.k && my_res >= 0.0)
      	res.k = -1.0;
    }
}

bool		CubeTroue::setCubeTroue(std::ifstream &file, int *line_nb)
{
  int		i;
  int		size;
  std::string	line;
  std::string	call;

  i = 0;
  while (i < ELEM_MAX_CUBETROUE && getline(file, line))
    {
      (*line_nb)++;
      if (i >= ELEM_MIN_CUBETROUE && !line.compare("END"))
	return (true);
      if (line.compare(0, 2, "##") != 0)
	{
	  if (line.size() < 5)
	    return (false);
	  if ((size = line.find(" = ") + 3) == std::string::npos)
	    return (false);
	  call = line.substr(0, size);
	  if (!this->ParseFunc[call])
	    return (false);
	  (this->*ParseFunc[call])(line.substr(size));
	  ++i;
	}
      else
	std::cout << line.c_str() + 3 << std::endl;
    }
  return (false);
}
