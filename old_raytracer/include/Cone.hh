//
// Cylinder.hh for generic in /home/coussi_n/raytracer/src/objects
//
// Made by Nelson Coussinet
// Login   <coussi_n@coussi-n-pc>
//
// Started on Thu Apr  3 19:49:25 2014 Nelson Coussinet
// Last update Tue May  6 12:12:01 2014 Nelson Coussinet
//

#ifndef				CONE_HH_
# define			CONE_HH_

# define ELEM_MAX_CONE	9
# define ELEM_MIN_CONE	5

# include			<sstream>
# include			<fstream>
# include			<map>
# include			"IObject.hh"
# include			"structs.h"

class				Cone : public IObject
{
  private:

    typedef void		(Cone::*ptr)(const std::string &);
    std::map<std::string, ptr>	ParseFunc;

    void			setRotX(const std::string &);
    void			setRotY(const std::string &);
    void			setRotZ(const std::string &);
    void			setX(const std::string &);
    void			setY(const std::string &);
    void			setZ(const std::string &);
    void			setCst(const std::string &);
    void			setRGB(const std::string &);
    void			setBrillance(const std::string &);
    void			setReflect(const std::string &);

  public:
    Cone();
    ~Cone() {};

    void			getKColor(t_view &, t_pos &, t_k &);
    void			getNormale(t_pos &, t_view &, t_view *);
    t_rgb			*getRGB(void);
    double			getBrillance(void);

    double			getReflect(void);

    void			setNumObj(int);
    int				getNumObj(void);
    double			calcDelta(double, double, double);
    Type			getType(void) const;
    bool			setCone(std::ifstream &, int *);
    void			setNormale(double k, t_view &, t_pos &);

  private:

    void			initParse(void);

    t_view			rot;
    t_view			normale;
    t_rgb			*rgb;
    int				cst;
    Rotation			rotation;
    double			tan_angle;
    double			brill;
};

#endif				/* !CONE_HH_ */
