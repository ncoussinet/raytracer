//
// Cylinder.hh for generic in /home/coussi_n/raytracer/src/objects
//
// Made by Nelson Coussinet
// Login   <coussi_n@coussi-n-pc>
//
// Started on Thu Apr  3 19:49:25 2014 Nelson Coussinet
// Last update Tue May  6 12:11:50 2014 Nelson Coussinet
//

#ifndef				CYLINDER_HH_
# define			CYLINDER_HH_

# define ELEM_MAX_CYLINDER	9
# define ELEM_MIN_CYLINDER	5

# include			<sstream>
# include			<fstream>
# include			<map>
# include			"IObject.hh"
# include			"structs.h"

class				Cylinder : public IObject
{
  private:

    typedef void		(Cylinder::*ptr)(const std::string &);
    std::map<std::string, ptr>	ParseFunc;

    void			setRotX(const std::string &);
    void			setRotY(const std::string &);
    void			setRotZ(const std::string &);
    void			setX(const std::string &);
    void			setY(const std::string &);
    void			setZ(const std::string &);
    void			setRay(const std::string &);
    void			setRGB(const std::string &);
    void			setBrillance(const std::string &);
    void			setReflect(const std::string &);

  public:
    Cylinder();
    ~Cylinder() {};

    void			getKColor(t_view &, t_pos &, t_k &);
    void			getNormale(t_pos &, t_view &, t_view *);
    t_rgb			*getRGB(void);
    void			setNumObj(int);
    int				getNumObj(void);
    double			getBrillance(void);

    double			getReflect(void);

    double			calcDelta(double, double, double);
    Type			getType(void) const;
    bool			setCylinder(std::ifstream &, int *);

  private:

    void			initParse(void);

    double			brill;
    t_view			rot;
    t_rgb			*rgb;
    int				ray;
    Rotation			rotation;
};

#endif				/* !CYLINDER_HH_ */
