//
// raytracer.hh for generic in /home/coussi_n/raytracer
//
// Made by Nelson Coussinet
// Login   <coussi_n@coussi-n-pc>
//
// Started on Tue Apr  1 11:33:33 2014 Nelson Coussinet
// Last update Mon May  5 15:43:12 2014 Nelson Coussinet
//

#ifndef				RAYTRACER_HH_
# define			RAYTRACER_HH_

# include			<list>
# include			<Xt/Window.hh>
# include			<map>
# include			<fstream>
# include			"camera.hh"
# include			"ObjectParse.hh"
# include			"Spot2.hh"
# include			"Light.hh"
# include			"Conf.hh"
# include			"calc.hh"
# include			"CartoonEffect.hh"

# define FOV			30

class				Raytracer
{
  private:

    typedef void		(Raytracer::*Pr)(std::ifstream &);
    std::map<std::string, Pr>	ParseFunc;

    void			WinSize(std::ifstream &);
    void			CamPos(std::ifstream &);
    void			ObjsCreate(std::ifstream &);
    void			SpotCreate(std::ifstream &);
    void			ConfType(std::ifstream &);

    void			prepareParse(void);

  public:
    Raytracer(void);
    Raytracer(const std::string &);

    ~Raytracer(void);

    int				calc(int xmin, int xmax);
    void			setLayer(Xt::Layer *);
    void			setScene(const std::string &);
    int				getLenght(void);
    int				getHeight(void);

  protected:

  private:

    void			openFile(void);
    unsigned int		sendRay(int x, int y);
    IObject			*check_closest(t_pos &, t_view &, int, t_k &);

    void			initRay(int x, int y, t_view *, t_view *);

    Xt::Layer			*img;
    std::string			path;
    std::list<IObject *>	objs;
    std::list<Spot2 *>		lum;
    int				_lenght;
    int				_height;
    int				semiLenght;
    int				semiHeight;
    Camera			_cam;
    int				line_nb;
    ObjectParse			Parseobj;
    t_view			view;

    CartoonEffect		cartoon;

    // light
    void			light(IObject *, t_pos &, t_view &, t_k &);
    void			initLight(t_pos &, t_view &, t_k &, t_rgb &);

    Light			my_light;

    // Reflect

    void			Reflect(t_view &, t_pos &, IObject *, t_k &);

    Conf			_conf;
};

#endif			/* RAYTRACER_HH_ */
