//
// Conf.hh for generic in /home/coussi_n/raytracer/src
//
// Made by Nelson Coussinet
// Login   <coussi_n@coussi-n-pc>
//
// Started on Fri Apr 11 15:31:00 2014 Nelson Coussinet
// Last update Mon May  5 16:14:56 2014 Nelson Coussinet
//

#ifndef				CONF_HH_
# define			CONF_HH_

# include			<fstream>
# include			<map>

# define ELEM_MAX_CONF		3
# define ELEM_MIN_CONF		1

class				Conf
{
  public:

    Conf(void);
    ~Conf(void);

    bool			setConf(std::ifstream &, int *);

    bool			_shadow;
    bool			_light;
    bool			_cartoon;

  private:

    typedef void		(Conf::*ptr)(const std::string &);
    std::map<std::string, ptr>	ParseFunc;

    void			ConfLight(const std::string &);
    void			ConfShadow(const std::string &);
    void			ConfCartoon(const std::string &);
};

#endif				/* !CONF_HH_ */
