//
// CartoonEffect.hh for generic in /home/coussi_n/raytracer/2new_ray/src
//
// Made by Nelson Coussinet
// Login   <coussi_n@coussi-n-pc>
//
// Started on Mon May  5 15:30:16 2014 Nelson Coussinet
// Last update Mon May  5 16:08:11 2014 Nelson Coussinet
//

#ifndef		CARTOONEFFECT_HH_
# define	CARTOONEFFECT_HH_

# include	"structs.h"

class		CartoonEffect
{
  public:
    CartoonEffect(void);
    ~CartoonEffect(void);

    void	tryCellShading(unsigned int *);
    int		toon(int, double);
};

#endif		/* !CARTOONEFFECT_HH_ */
