//
// Spot.hh for generic in /home/coussi_n/raytracer/src/objects
//
// Made by Nelson Coussinet
// Login   <coussi_n@coussi-n-pc>
//
// Started on Sat Apr  5 19:58:28 2014 Nelson Coussinet
// Last update Sat Apr  5 21:26:42 2014 Nelson Coussinet
//

#ifndef				SPOT_HH_
# define			SPOT_HH_

# include			<fstream>
# include			<map>
# include			"structs.h"
# include			<istream>
# include			<ostream>

# define ELEM_MAX_SPOT	7
# define ELEM_MIN_SPOT	3

class				Spot
{
  public:
    Spot(void);
    ~Spot(void);

    bool			setSpot(std::ifstream &, int *);
    t_pos			&getSpotPos(void);

  private:

    typedef void		(Spot::*ptr)(const std::string &);
    std::map<std::string, ptr>	ParseFunc;

    void			SpotX(const std::string &);
    void			SpotY(const std::string &);
    void			SpotZ(const std::string &);

    t_pos			SpotPos;
};

#endif /* SPOT_HH_ */
