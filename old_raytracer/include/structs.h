/*
** structs.h for generic in /home/coussi_n/raytracer/src
**
** Made by Nelson Coussinet
** Login   <coussi_n@coussi-n-pc>
**
** Started on Wed Apr  2 14:04:37 2014 Nelson Coussinet
** Last update Tue May  6 10:13:19 2014 Nelson Coussinet
*/

#ifndef			STRUCTS_H_
# define		STRUCTS_H_

typedef struct		s_pos
{
    double		x;
    double		y;
    double		z;
}			t_pos;

typedef struct		s_view
{
    double		x;
    double		y;
    double		z;
}			t_view;

typedef struct		s_rgb
{
    int			r;
    int			g;
    int			b;
    void		operator+=(s_rgb const &color) {r += color.r; g += color.g; b += color.b;}
    void		operator+=(s_rgb const *color) {r += color->r; g += color->g; b += color->b;}
    void		operator/=(int const div) {r /= div; g /= div; b /= div;}
}			t_rgb;

typedef struct		s_k
{
    unsigned int	color;
    double		k;
    int			reflect;
}			t_k;

#endif			/* !STRUCTS_H_ */
