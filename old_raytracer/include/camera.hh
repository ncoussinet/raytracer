//
// camera.hh for generic in /home/coussi_n/raytracer/src
//
// Made by Nelson Coussinet
// Login   <coussi_n@coussi-n-pc>
//
// Started on Wed Apr  2 14:06:46 2014 Nelson Coussinet
// Last update Sat Apr  5 21:26:37 2014 Nelson Coussinet
//

#ifndef				CAMERA_HH_
# define			CAMERA_HH_

# include			<fstream>
# include			<map>
# include			"structs.h"

class				Camera
{
  public:
    Camera(void);
    ~Camera(void);

    bool			setPosition(std::ifstream &, int *);
    const t_pos			&getPosition(void) const;

  private:

    typedef void		(Camera::*ptr)(const std::string &);
    std::map<std::string, ptr>	ParseFunc;

    void			CamX(const std::string &);
    void			CamY(const std::string &);
    void			CamZ(const std::string &);

    t_pos			CamPos;
};

#endif				/* !CAMERA_HH_ */
