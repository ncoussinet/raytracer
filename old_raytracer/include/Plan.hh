//
// Plan.hh for generic in /home/coussi_n/raytracer/src
//
// Made by Nelson Coussinet
// Login   <coussi_n@coussi-n-pc>
//
// Started on Wed Apr  2 18:58:00 2014 Nelson Coussinet
// Last update Tue May  6 12:11:41 2014 Nelson Coussinet
//

#ifndef			PLAN_HH_
# define		PLAN_HH_

# define ELEM_MAX_PLAN		8
# define ELEM_MIN_PLAN		2

# include			<sstream>
# include			<fstream>
# include			<map>
# include			"IObject.hh"
# include			"structs.h"

class				Plan : public IObject
{
  private:

    typedef void		(Plan::*ptr)(const std::string &);
    std::map<std::string, ptr>	ParseFunc;

    void			setRotX(const std::string &);
    void			setRotY(const std::string &);
    void			setRotZ(const std::string &);
    void			setX(const std::string &);
    void			setY(const std::string &);
    void			setZ(const std::string &);
    void			setRGB(const std::string &);
    void			setBrillance(const std::string &);
    void			setReflect(const std::string &);

  public:
    Plan();
    ~Plan() {};

    Type			getType(void) const;
    bool			setPlan(std::ifstream &, int *);
    void			getNormale(t_pos &, t_view &, t_view *);
    t_rgb			*getRGB(void);
    void			setNumObj(int);
    int				getNumObj(void);

    void			getKColor(t_view &, t_pos &, t_k &);
    double			getBrillance(void);

    double			getReflect(void);

  private:

    void			initParse(void);

    Rotation			rotation;
    t_view			rot;
    t_rgb			*rgb;
    double			brill;
};

#endif			/* !PLAN_HH_ */
