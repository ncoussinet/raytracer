//
// ObjectParse.hh for generic in /home/coussi_n/raytracer/src
//
// Made by Nelson Coussinet
// Login   <coussi_n@coussi-n-pc>
//
// Started on Wed Apr  2 15:36:46 2014 Nelson Coussinet
// Last update Wed Apr 16 19:03:29 2014 Nelson Coussinet
//

#ifndef				OBJECTPARSE_HH_
# define			OBJECTPARSE_HH_

# include			<fstream>
# include			"Circle.hh"
# include			"Plan.hh"
# include			"Cylinder.hh"
# include			"Cone.hh"
# include			"CubeTroue.hh"

class				ObjectParse
{
  private:

    typedef IObject		*(ObjectParse::*ptr)(std::ifstream &, int *);
    std::map<std::string, ptr>	ParseFunc;

    IObject			*my_Circle(std::ifstream &, int *);
    IObject			*my_Cylinder(std::ifstream &, int *);
    IObject			*my_Cone(std::ifstream &, int *);
    IObject			*my_Plan(std::ifstream &, int *);
    IObject			*my_CubeTroue(std::ifstream &, int *);

  public:
    ObjectParse(void);
    ~ObjectParse(void);

    IObject			*getObj(std::ifstream &, int *);
};

#endif				/* !OBJECTPARSE_HH_ */
