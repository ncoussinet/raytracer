//
// calc.hh for generic in /home/coussi_n/raytracer/2new_ray/src
//
// Made by Nelson Coussinet
// Login   <coussi_n@coussi-n-pc>
//
// Started on Mon May  5 12:47:43 2014 Nelson Coussinet
// Last update Tue May  6 11:34:41 2014 Nelson Coussinet
//

#ifndef		CALC_HH_
# define	CALC_HH_

# include	"structs.h"

double		mul_vect(t_view *, t_view *);
double		vect_sum(t_view *);
double		magnitude(t_view *);
void		normalize(t_view *, t_view *);
void		new_cam_pos(t_pos *, double, t_view *, t_pos *);
void		new_vect_sub(t_pos *, t_pos *, t_view *);
void		new_color_div(t_rgb *, int);
void		check_too_strong(t_rgb *);
void		transformColorToRGB(t_rgb &, unsigned int);

#endif		/* !CALC_HH_ */
