//
// Rotation.hh for generic in /home/coussi_n/raytracer/src/objects
//
// Made by Nelson Coussinet
// Login   <coussi_n@coussi-n-pc>
//
// Started on Thu Apr  3 17:21:56 2014 Nelson Coussinet
// Last update Fri Apr 25 18:07:47 2014 Nelson Coussinet
//

#ifndef		ROTATION_HH_
# define	ROTATION_HH_

# include	"structs.h"

class		Rotation
{
  public:
    Rotation(void);
    ~Rotation(void);

    void	rotate(t_view *, double, double, double);

  private:

    void	rotate_x(double [3][3], t_view *, double);
    void	rotate_y(double [3][3], t_view *, double);
    void	rotate_z(double [3][3], t_view *, double);
    void	mul_matrix(double [3][3], t_view *);
};

#endif		/* !ROTATION_HH_ */
