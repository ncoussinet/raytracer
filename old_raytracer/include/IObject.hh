//
// IObject.hh for generic in /home/coussi_n/raytracer/include
//
// Made by Nelson Coussinet
// Login   <coussi_n@coussi-n-pc>
//
// Started on Tue Apr  1 12:47:15 2014 Nelson Coussinet
// Last update Tue May  6 12:11:23 2014 Nelson Coussinet
//

#ifndef			IOBJECT_HH_
# define		IOBJECT_HH_

# include		"structs.h"
# include		"Rotation.hh"

enum			Type
{
  Unknown = 0,
  CIRCLE = 1,
  PLAN,
  CYLINDER,
  CONE,
  CUBETROUE
};

class			IObject
{
  public:
    virtual		~IObject() {};
    virtual Type	getType(void) const = 0;
    virtual t_rgb	*getRGB(void) = 0;
    virtual void	getNormale(t_pos &, t_view &, t_view *) = 0;
    virtual void	getKColor(t_view &, t_pos &, t_k &) = 0;
    virtual int		getNumObj(void) = 0;
    virtual void	setNumObj(int) = 0;
    virtual double	getBrillance(void) = 0;
    virtual double	getReflect(void) = 0;

    // virtual t_view	&getNormale(t_view &) = 0;

    t_pos		pos;
    int			numb_obj;

  protected:

    Type	type;
    double	reflect;
};


#endif			/* IOBJECT_HH_ */
