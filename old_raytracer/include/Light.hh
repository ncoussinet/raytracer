//
// Light.hh for generic in /home/coussi_n/raytracer/include
//
// Made by Nelson Coussinet
// Login   <coussi_n@coussi-n-pc>
//
// Started on Thu Apr 10 10:21:01 2014 Nelson Coussinet
// Last update Thu May  1 12:29:18 2014 Nelson Coussinet
//

#ifndef		LIGHT_HH_
# define	LIGHT_HH_

# include	"IObject.hh"
# include	"structs.h"

class		Light
{
  public:
    Light(void);
    ~Light(void);

    double	getLightColor(t_view &vect, t_rgb *color, t_view &vect_n, t_rgb *);
    void	getReflect(t_rgb *color, double reflec, t_rgb *color_obj);
    void	getShine(double &, t_rgb *, t_rgb *, double);
};

#endif /* LIGHT_HH_ */
