//
// Circle.hh for generic in /home/coussi_n/raytracer/include
//
// Made by Nelson Coussinet
// Login   <coussi_n@coussi-n-pc>
//
// Started on Tue Apr  1 12:50:43 2014 Nelson Coussinet
// Last update Tue May  6 12:11:33 2014 Nelson Coussinet
//

#ifndef				CIRCLE_HH_
# define			CIRCLE_HH_

# define ELEM_MAX_CIRCLE	9
# define ELEM_MIN_CIRCLE	5

# include			<sstream>
# include			<fstream>
# include			<map>
# include			"IObject.hh"
# include			"structs.h"

class				Circle : public IObject
{
  private:

    typedef void		(Circle::*ptr)(const std::string &);
    std::map<std::string, ptr>	ParseFunc;

    void			setRotX(const std::string &);
    void			setRotY(const std::string &);
    void			setRotZ(const std::string &);
    void			setX(const std::string &);
    void			setY(const std::string &);
    void			setZ(const std::string &);
    void			setRay(const std::string &);
    void			setRGB(const std::string &);
    void			setBrillance(const std::string &);
    void			setReflect(const std::string &);

  public:
    Circle();
    ~Circle() {};

    void			setNumObj(int);
    int				getNumObj(void);
    void			getNormale(t_pos &, t_view &, t_view *);
    void			getKColor(t_view &, t_pos &, t_k &);
    double			calcDelta(double, double, double);
    Type			getType(void) const;
    bool			setCircle(std::ifstream &, int *);
    t_rgb			*getRGB(void);
    double			getBrillance(void);
    double			getReflect(void);

  private:

    void			initParse(void);

    t_view			rot;
    t_rgb			*rgb;
    int				ray;
    Rotation			rotation;
    double			brill;
};

#endif				 /* !CIRCLE_HH_ */
