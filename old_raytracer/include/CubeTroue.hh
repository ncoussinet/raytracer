//
// CubeTroue.hh for generic in /home/coussi_n/raytracer/src/objects
//
// Made by Nelson Coussinet
// Login   <coussi_n@coussi-n-pc>
//
// Started on Wed Apr 16 16:57:00 2014 Nelson Coussinet
// Last update Tue May  6 12:12:08 2014 Nelson Coussinet
//

#ifndef				CUBETROUE_HH_
# define			CUBETROUE_HH_

# define ELEM_MAX_CUBETROUE	8
# define ELEM_MIN_CUBETROUE	5

# include			<sstream>
# include			<fstream>
# include			<map>
# include			"IObject.hh"
# include			"structs.h"
# include			"CalculResolv.hh"

class				CubeTroue : public IObject
{
  private:

    typedef void		(CubeTroue::*ptr)(const std::string &);
    std::map<std::string, ptr>	ParseFunc;

    void			setRotX(const std::string &);
    void			setRotY(const std::string &);
    void			setRotZ(const std::string &);
    void			setX(const std::string &);
    void			setY(const std::string &);
    void			setZ(const std::string &);
    void			setRGB(const std::string &);
    void			setReflect(const std::string &);

  public:
    CubeTroue();
    ~CubeTroue() {};

    void			getKColor(t_view &, t_pos &, t_k &);
    void			getNormale(t_pos &, t_view &, t_view *);
    t_rgb			*getRGB(void);

    void			setNumObj(int);
    int				getNumObj(void);
    Type			getType(void) const;
    bool			setCubeTroue(std::ifstream &, int *);
    void			setNormale(double k, t_view &, t_pos &);
    double			getBrillance(void);

    double			getReflect(void);

  private:

    double			calcDelta(double, double, double);
    void			check_troue(t_view &, t_pos &, double *);

    void			initParse(void);

    t_view			rot;
    t_view			normale;
    t_rgb			*rgb;
    Rotation			rotation;
};

#endif				/* !CUBETROUE_HH_ */
