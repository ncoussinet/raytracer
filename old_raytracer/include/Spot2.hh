//
// camera.hh for generic in /home/coussi_n/raytracer/src
//
// Made by Nelson Coussinet
// Login   <coussi_n@coussi-n-pc>
//
// Started on Wed Apr  2 14:06:46 2014 Nelson Coussinet
// Last update Fri Apr 18 17:11:53 2014 Nelson Coussinet
//

#ifndef				SPOT2_HH_
# define			SPOT2_HH_

# include			<fstream>
# include			<map>
# include			"structs.h"

class				Spot2
{
  public:
    Spot2(void);
    ~Spot2(void);

    bool			setPosition(std::ifstream &, int *);
    const t_pos			&getPosition(void) const;

    t_rgb			*getColor(void);

  private:

    typedef void		(Spot2::*ptr)(const std::string &);
    std::map<std::string, ptr>	ParseFunc;

    void			CamX(const std::string &);
    void			CamY(const std::string &);
    void			CamZ(const std::string &);
    void			setRGB(const std::string &);

    t_rgb			*rgb;
    t_pos			CamPos;
};

#endif				/* !CAMERA_HH_ */
